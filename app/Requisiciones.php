<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Requisiciones extends Model
{
    use SoftDeletes;
   protected $table="requisiciones";
   protected $fillable=["nombre","observaciones","usuario_id"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
