@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
	 <div id="viewformato" class="col-lg-12">			
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Consulta Cartera</h3>
	        </div>
	        <div class="panel-body">
	        	<div class="row">
	 				<div   id="listadofacturas"  class="col-lg-12">
	 					<div class="panel panel-info">
			 					<div class="panel-heading">
			           				 <h3 class="panel-title">Seleccionar cliente</h3>
			        			</div>
			 					<div class="panel-body">
								         <div class="table-responsive">
								       		<table id="tblclientes" class="table table-triped" >      <thead>  		
									    				 <tr>
									    				    <th>
								        						Nit
								        					</th>
								        					<th>
								        						Razón social
								        					</th>
								        					<th>
								        						Teléfonos
								        					</th>
								        					<th>
								        						Email
								        					</th>
								        					<th width="10%" align="center">
								        						Seleccionar
								        					</th>
								        				</tr>
								        		</thead>
								        		<tbody>	
								        					<?php
								        						
								        						for ($i=0; $i < count($lista); $i++) { 
								        							echo "<tr>";
								        							echo "<td>".$lista[$i]->nit."</td><td>".$lista[$i]->razonsocial."</td><td>".$lista[$i]->telefonos." ".$lista[$i]->celular."</td><td>".$lista[$i]->email."</td><td><label onclick=consultar(".$lista[$i]->id.") class='btn btn-info btn-block'><i class='fa fa-check-circle'> Seleccionar</label></td>";
								        							echo "</tr>";
								        						}
								        					?>
								        		</tbody>	
								          	</table>
								        </div>
								    </div>
					    </div>
				    </div>
				</div>        
	    </div>
	   {!!Form::close()!!} 
	 </div>
	
</div>
</div>

@endsection	
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({

    	 "language":	
            {
            	 "url": "/assets/js/pluginspanishtabla"
            }
    });
});	
	
</script>
	{!!Html::script('assets/js/cartera.js')!!}
@endsection

