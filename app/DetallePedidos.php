<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetallePedidos extends Model
{
   protected $table="detallepedido";
   protected $fillable=["pedido_id","producto_id","cantidad","precio","terminado","entregado","rechazado"]; 	
}
