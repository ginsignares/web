<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class PedidosController extends Controller
{
  public function facturapendientes(Request $requests){
     $tabla = DB::table('factura')->select('factura.id as facid','detallefactura.cantidad','detallefactura.terminado')->join('detallefactura','detallefactura.factura_id','=','factura.id')->get();
        $cantidad=0;
        $terminado=0;
        for ($i=0; $i < count($tabla) ; $i++) { 
            $cantidad+=$tabla[$i]->cantidad;
            $terminado+=$tabla[$i]->terminado;


                  $saldo=$cantidad-$terminado;
            if ($saldo==0)
            {
                 $pedido=\App\Factura::where('id',$tabla[$i]->facid)->first();
                 $pedido->estado="Terminado";
                 $pedido->terminado=1;
                 $pedido->save();
            }  
        }

     return response()->json(['valid'=>true]);  
  }
  public function listadofacturas(Request $requests){
     $reporte= array();
     $detalle = DB::table('productos')->select('productos.id as producto_id','productos.nombre')->join('detallefactura','detallefactura.producto_id','=','productos.id')->OrderBy('productos.id')->distinct()->get();
     
     if ($detalle!=null){
        for ($i=0; $i < count($detalle); $i++) { 
              $saldo = DB::table('detallefactura')->select(DB::raw('SUM(detallefactura.cantidad-detallefactura.terminado) as saldo'))->where('producto_id',$detalle[$i]->producto_id)->first();
              if ($saldo!=null){
                if ($saldo->saldo>0)
                array_push($reporte, ['producto_id'=>$detalle[$i]->producto_id,'nombre'=>$detalle[$i]->nombre,'saldo'=>$saldo->saldo]);
              }

        }
     }

      return view('pedidos.listadofacturas',['lista'=>$reporte]);
  }  

  public function listadofacturasporfecha(Request $requests){
      return view('pedidos.listadopedidosporfecha',['lista'=>array()]);
  

  }


      





  public function generarPDF(Request $requests){
    $datosbasico=\App\Datosbasicos::all();
    $factura=\App\Factura::where('id',$requests->factura)->first();
    $cliente=\App\Clientes::where('id',$factura->cliente_id)->first();
    $detalle = DB::table('productos')->select('productos.codigo','productos.nombre','detallefactura.cantidad','detallefactura.precio')->join('detallefactura','detallefactura.producto_id','=','productos.id')->where('detallefactura.factura_id',$factura->id)->get();

     $pdf=\PDF::loadView('pedidos.pdf',['factura'=>$requests->factura,'datosbasico'=>$datosbasico,'datosfactura'=>$factura,'cliente'=>$cliente,'detalle'=>$detalle]);
       return $pdf->stream('factura.pdf');

  }  
   public function generarPDFresumen(Request $requests){
    $datosbasico=\App\Datosbasicos::all();
    $factura=\App\Factura::where('id',$requests->factura)->first();
    $cliente=\App\Clientes::where('id',$factura->cliente_id)->first();
    $detalle = DB::table('productos')->select('productos.codigo','productos.nombre','detallefactura.cantidad','detallefactura.precio','detallefactura.terminado','detallefactura.entregado')->join('detallefactura','detallefactura.producto_id','=','productos.id')->where('detallefactura.factura_id',$factura->id)->get();

     $pdf=\PDF::loadView('pedidos.pdfresumen',['factura'=>$requests->factura,'datosbasico'=>$datosbasico,'datosfactura'=>$factura,'cliente'=>$cliente,'detalle'=>$detalle]);
       return $pdf->stream('factura.pdf');

  }  
  public function abono(Request $requests){
    return view('pedidos.abono',['total'=>$requests->total]);
  }

   public function actulizarproductoglobal(Request $requests){
        $datos1=array();
        $datos=DB::table('detallefactura')->select()->where('factura_id',$requests->factura)->get();

           $kardex= new \App\Kardex;
            $kardex->concepto="ENTREGA PEDIDO FAC #".$requests->factura;
            $kardex->operacion=1;
            $kardex->localizacion=3;
            $kardex->save();

        $registros=count($datos);

        for ($i=0; $i < $registros; $i++) { 
            //1. dar terminado en la factura
            $saldoter=$datos[$i]->cantidad-$datos[$i]->terminado;
            if ($saldoter>0){
              $detfac=\App\DetalleFactura::where('id',$datos[$i]->id)->first();
              $detfac->terminado=$datos[$i]->terminado+$saldoter;
              $detfac->save();

              $detallekardex=new \App\DetalleKardex;
              $detallekardex->kardex_id=$kardex->id;
              $detallekardex->producto_id=$datos[$i]->producto_id;
              $detallekardex->cantidad=$saldoter;
              $detallekardex->precio=0;
              $detallekardex->save();

              $saldo=\App\Saldos::where('producto_id',$datos[$i]->producto_id)->first();
              $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->where('localizacion_id',3)->first();
              $saldo_localizacion->entradas=$saldo_localizacion->entradas+$saldoter;
              $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
              $saldo_localizacion->saldos=$nuevosaldo;
              $saldo_localizacion->save();
            }

        }
        
        $pedido=\App\Factura::where('id',$requests->factura)->first();
        $pedido->estado="Terminado";
        $pedido->terminado=1;
        $pedido->save();

    

     
        //return response()->json(['texto'=>$datos1]);
         return response()->json(['valid'=>true]);

   }


 public function actulizarproductoglobal2(Request $requests){
        $datos1=array();
        $datos=DB::table('detallefactura')->select()->where('factura_id',$requests->factura)->get();

         $pedido=\App\Factura::where('id',$requests->factura)->first();

        $despacho= new \App\Despachos;
        $despacho->cliente_id=$pedido->cliente_id;
        $despacho->usuario_id=\Auth::user()->id;
        $despacho->save();

         $kardex= new \App\Kardex;
         $kardex->concepto="DESPACHO #".$despacho->id;
         $kardex->operacion=2;
         $kardex->localizacion=3;
         $kardex->save();

        $registros=count($datos);

        for ($i=0; $i < $registros; $i++) { 

            $saldodes=$datos[$i]->terminado-$datos[$i]->entregado;

            if ($saldodes>0){
            
            $detalledespacho=new \App\DetalleDespachos;
            $detalledespacho->despacho_id=$despacho->id;
            $detalledespacho->detalle_id=$datos[$i]->id;
            $detalledespacho->cantidad=$saldodes;
            $detalledespacho->save();


            
            $detfac=\App\DetalleFactura::where('id',$datos[$i]->id)->first();
            $detfac->entregado=$datos[$i]->entregado+$saldodes;
            $detfac->save();

            $detallekardex=new \App\DetalleKardex;
            $detallekardex->kardex_id=$kardex->id;
            $detallekardex->producto_id=$datos[$i]->producto_id;
            $detallekardex->cantidad=$saldodes;
            $detallekardex->precio=0;
            $detallekardex->save();

            $saldo=\App\Saldos::where('producto_id',$datos[$i]->producto_id)->first();
            $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->where('localizacion_id',3)->first();
            $saldo_localizacion->salidas=$saldo_localizacion->salidas+$saldodes;
            $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
            $saldo_localizacion->saldos=$nuevosaldo;
            $saldo_localizacion->save();

           } 

        }
        


        $tabla = DB::table('factura')->select('detallefactura.cantidad','detallefactura.entregado')->join('detallefactura','detallefactura.factura_id','=','factura.id')->where('factura.id',$requests->factura)->get();
        $cantidad=0;
        $entregado=0;
        for ($i=0; $i < count($tabla) ; $i++) { 
            $cantidad+=$tabla[$i]->cantidad;
            $entregado+=$tabla[$i]->entregado;
        }

        $saldo=$cantidad-$entregado;
        if ($saldo==0)
        {
             $pedido=\App\Factura::where('id',$requests->factura)->first();
             $pedido->estado="Entregado";
             $pedido->terminado=2;
             $pedido->save();
        }  
        

    

     
        //return response()->json(['texto'=>$datos1]);
         return response()->json(['valid'=>true]);

   }

   public function actulizarproducto(Request $requests){
        $datos=\App\DetalleFactura::where('id',$requests->detalle_id)->first();
        $saldo=$datos->terminado+$requests->cantidad;
        $datos->terminado=$saldo;
        $datos->save();

        $kardex= new \App\Kardex;
        $kardex->concepto="TERMINADO DEL PEDIDO FAC #".$datos->factura_id;
        $kardex->operacion=1;
        $kardex->localizacion=3;
        $kardex->save();

        $detallekardex=new \App\DetalleKardex;
        $detallekardex->kardex_id=$kardex->id;
        $detallekardex->producto_id=$datos->producto_id;
        $detallekardex->cantidad=$requests->cantidad;
        $detallekardex->precio=0;
        $detallekardex->save();

        $saldo=\App\Saldos::where('producto_id',$datos->producto_id)->first();
        $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->where('localizacion_id',3)->first();
        $saldo_localizacion->entradas=$saldo_localizacion->entradas+$requests->cantidad;
        $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
        $saldo_localizacion->saldos=$nuevosaldo;
        $saldo_localizacion->save();

        $tabla = DB::table('factura')->select('detallefactura.cantidad','detallefactura.terminado')->join('detallefactura','detallefactura.factura_id','=','factura.id')->where('factura.id',$datos->factura_id)->get();
        $cantidad=0;
        $terminado=0;
        for ($i=0; $i < count($tabla) ; $i++) { 
            $cantidad+=$tabla[$i]->cantidad;
            $terminado+=$tabla[$i]->terminado;
        }

        $saldo=$cantidad-$terminado;
        if ($saldo==0)
        {
             $pedido=\App\Factura::where('id',$datos->factura_id)->first();
             $pedido->estado="Terminado";
             $pedido->terminado=1;
             $pedido->save();
        }  
        return response()->json(['valid'=>true]);

   }


    public function formato(Request $requests){
        if ($requests['formato_id']==0){
          $tabla = DB::table('clientes')->select()->where('id',$requests->cliente_id)->where('deleted_at',null)->first(); 
             return view('pedidos.formato',['datos'=>$tabla,'fecha'=>date("Y-m-d")]);
        }
       
    }   
   
    public function controlpedidosporfecha(Request $requests){

        $tabla = DB::table('factura')->select('factura.id as factura_id','clientes.razonsocial','factura.fechaentrega','factura.created_at','factura.estado','detallefactura.id as detalle_id','productos.nombre','detallefactura.cantidad','detallefactura.terminado')->join("detallefactura","detallefactura.factura_id","=","factura.id")->join('clientes','clientes.id','=','factura.cliente_id')->join('productos','productos.id','=','detallefactura.producto_id')->whereBetween('factura.fechaentrega',[$requests->fi." 00:00:00",$requests->ff." 23:59:59"])->where('factura.deleted_at',null)->OrderBy('factura.id')->get();

      return response()->json(['lista'=>$tabla]);
      
    }  

    public function controlpedidos(Request $requests){
        $tabla = DB::table('factura')->select('factura.id as factura_id','clientes.razonsocial','factura.fechaentrega','factura.created_at','factura.estado','detallefactura.id as detalle_id','productos.nombre','detallefactura.cantidad','detallefactura.terminado')->join("detallefactura","detallefactura.factura_id","=","factura.id")->join('clientes','clientes.id','=','factura.cliente_id')->join('productos','productos.id','=','detallefactura.producto_id')->where('detallefactura.producto_id',$requests->producto_id)->where('factura.deleted_at',null)->OrderBy('factura.fechaentrega')->get();

      return view('pedidos.controlpedido',['lista'=>$tabla]);
    }   

    public function obtenerproductosfacturar(Request $requests){
        $cliente=\App\Clientes::where('id',$requests->cliente_id)->first();
        $tabla = DB::table('pedidos')->select('pedidos.id as pedido_id','clientes.razonsocial','pedidos.fechaentrega','pedidos.created_at','pedidos.estado','detallepedido.id as detalle_id','productos.nombre','detallepedido.cantidad','detallepedido.terminado','detallepedido.entregado','detallepedido.rechazado')->join("detallepedido","detallepedido.pedido_id","=","pedidos.id")->join('clientes','clientes.id','=','pedidos.cliente_id')->join('productos','productos.id','=','detallepedido.producto_id')->where('pedidos.deleted_at',null)->where('pedidos.cliente_id',$requests->cliente_id)->get();
        return view('pedidos.listadoproductosfactura',['lista'=>$tabla,'cliente'=>$cliente]);
    }
    public function facturar(Request $requests){
      $tabla = DB::table('clientes')->select()->where('deleted_at',null)->get(); 

             return view('pedidos.facturar',['lista'=>$tabla]);
    } 
    public function listadopedidos(Request $requests){
       
         $tabla = DB::table('factura')->select('factura.id as factura_id','clientes.razonsocial','factura.fechaentrega','factura.created_at','factura.estado')->join('clientes','clientes.id','=','factura.cliente_id')->where('factura.deleted_at',null)->get();  
             return view('pedidos.listadopedidos',['lista'=>$tabla]);
        
       
    }   
     public function listadopedidos1(Request $requests){
       
         $tabla = DB::table('factura')->select('factura.id as factura_id','clientes.razonsocial','factura.fechaentrega','factura.created_at','factura.estado','factura.terminado')->join('clientes','clientes.id','=','factura.cliente_id')->where('factura.deleted_at',null)->get();  
             return view('pedidos.listadopedidos2',['lista'=>$tabla]);
        
       
    } 
    public function formatoproductos(Request $requests){
             $tabla = DB::table('productos')->select()->where('fabricado','Si')->where('deleted_at',null)->get(); 
             return view('pedidos.listadoproducto',['lista'=>$tabla]);
    }    
    public function obtenerproducto(Request $requests){
             $tabla = DB::table('productos')->select()->where('id',$requests->producto_id)->get(); 
             return $tabla;
    }  
      public function guardar(Request $requests){
             $cierre= DB::table('cierres')->select()->OrderBy('id','desc')->first();
         
             if ($cierre==null)
                return response()->json(['mensaje'=>'La caja no se encuentra abierta para registrar el abono.']);
             $factura= new \App\Factura;
             $factura->cliente_id=$requests->cliente_id;

             if ($requests->abono==$requests->valor)
                $factura->formapago=0;
             else   
                $factura->formapago=1;
             
             $factura->estado="Pendiente";
             $factura->descuento=$requests->descuento;
             $factura->total=$requests->valor;
             $factura->abonado=$requests->abono;
             
             $factura->usuario_id=\Auth::user()->id;
             $factura->fechaentrega=$requests->fechaentrega;
             $factura->save();
             $nmofactura=$factura->id;

                $documento=new \App\Documentos;
                $documento->concepto_id=2;
                $documento->cierre_id=$cierre->id;
                $documento->detalle="FAC # ".$nmofactura;
                $documento->valor=$requests->abono;
                $documento->usuario_id=\Auth::user()->id;
                $documento->save();
                
            
             for ($i=0; $i < count($requests->productos) ; $i++) {
                if ($requests->productos[$i]['estado']==0){ 
                  $detalle=new \App\DetalleFactura;  
                  $detalle->factura_id=$nmofactura;
                  $detalle->producto_id=$requests->productos[$i]['producto_id'];
                  $detalle->cantidad=$requests->productos[$i]['cantidad'];
                  $detalle->precio=$requests->productos[$i]['precio'];
                  $detalle->save();

                }   
             }

              return response()->json(['valid'=>true,'factura'=>$nmofactura]);

      }
       public function guardarpedido(Request $requests){
             $pedido= new \App\Pedidos;
             $pedido->cliente_id=$requests->cliente_id;
             $pedido->fechaentrega=$requests->fechaentrega;
             $pedido->observaciones=$requests->observaciones;
             $pedido->estado="Pendiente";
             $pedido->save();
             $nmopedido=$pedido->id;
             
             for ($i=0; $i < count($requests->productos) ; $i++) {
                if ($requests->productos[$i]['estado']==0){ 
                  $detalle=new \App\DetallePedidos;  
                  $detalle->pedido_id=$nmopedido;
                  $detalle->producto_id=$requests->productos[$i]['producto_id'];
                  $detalle->cantidad=$requests->productos[$i]['cantidad'];
                  $detalle->precio=$requests->productos[$i]['precio'];
                  $detalle->save();
                }   
             }

             return response()->json(['valid'=>true]);
      }
        

      public function guardarfactura(Request $requests){
            $cierre= DB::table('cierres')->select()->OrderBy('id','desc')->first();
           
             $factura= new \App\Factura;
             $factura->cliente_id=$requests->cliente_id;
             $factura->formapago=$requests->formapago;
             if ($requests->formapago==0)
                $factura->estado="Pagada";
             else
                $factura->estado="Pendiente";
             $factura->descuento=$requests->descuento;
             $factura->total=$requests->valor;
             $factura->usuario_id=\Auth::user()->id;
             $factura->save();
             $nmofactura=$factura->id;

            if ($requests->formapago==0){
                $documento=new \App\Documentos;
                $documento->concepto_id=1;
                $documento->cierre_id=$cierre->id;
                $documento->detalle="FAC # ".$nmofactura;
                $documento->valor=$requests->valor-$requests->descuento;
                $documento->usuario_id=\Auth::user()->id;
                $documento->save();
            }    
             $kardex= new \App\Kardex;
             $kardex->concepto="VENTA FAC #".$nmofactura;
             $kardex->operacion=2;
             $kardex->localizacion=3;
             $kardex->save();

             for ($i=0; $i < count($requests->productos) ; $i++) {
               
                if ($requests->productos[$i]['cantidad']>0){ 

                  $detallepedido=\App\DetallePedidos::where('id',$requests->productos[$i]['detalle_id'])->first();
                  $detallepedido->entregado+=$requests->productos[$i]['cantidad'];
                  $detallepedido->save();

                  $pedidofactura=\App\FacturaPedido::where('factura_id',$nmofactura)->where('pedido_id',$detallepedido->pedido_id)->first();
                  if ($pedidofactura==null){
                       $pedidofactura=new \App\FacturaPedido;
                       $pedidofactura->factura_id=$nmofactura;
                       $pedidofactura->pedido_id=$detallepedido->pedido_id;
                       $pedidofactura->save();
                  }
                  $detalle=new \App\DetalleFactura;  
                  $detalle->factura_id=$nmofactura;
                  $detalle->producto_id=$detallepedido->producto_id;
                  $detalle->cantidad=$requests->productos[$i]['cantidad'];
                  $detalle->precio=$detallepedido->precio;
                  $detalle->save();

                  $detallekardex=new \App\DetalleKardex;
                  $detallekardex->kardex_id=$kardex->id;
                  $detallekardex->producto_id=$detallepedido->producto_id;
                  $detallekardex->cantidad=-$requests->productos[$i]['cantidad'];
                  $detallekardex->precio=$detallepedido->precio;
                  $detallekardex->save();

                  $saldo=\App\Saldos::where('producto_id',$detallepedido->producto_id)->first();
                  $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->where('localizacion_id',3)->first();
                  $saldo_localizacion->salidas=$saldo_localizacion->salidas+$requests->productos[$i]['cantidad'];
                  $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
                  $saldo_localizacion->saldos=$nuevosaldo;
                  $saldo_localizacion->save();
                }  
                   
             }
             
             return response()->json(['valid'=>true]);
      }

      public function obtenerdetallepedido(Request $requests){

            $detallepedido=\App\DetallePedidos::where('id',$requests->detalle_id)->get();
            return $detallepedido;
      }
      
}
