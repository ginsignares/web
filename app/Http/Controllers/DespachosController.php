<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
class DespachosController extends Controller
{
    public function despacho(){
        $clientes=\App\Clientes::where('deleted_at',null)->get();
        return view('despachos.despachos',['lista'=>$clientes]);
   }

   public function listado(Request $request){
        $tabla = DB::table('factura')->select('factura.id as factura_id','factura.created_at','detallefactura.cantidad','detallefactura.terminado','detallefactura.entregado','productos.nombre','factura.cliente_id','clientes.razonsocial','detallefactura.id as detalle_id')->join('detallefactura','detallefactura.factura_id','=','factura.id')->join('productos','productos.id','=','detallefactura.producto_id')->join('clientes','clientes.id','=','factura.cliente_id')->where('factura.cliente_id',$request->cliente_id)->where('factura.deleted_at',null)->get(); 

        $clientes=\App\Clientes::where('id',$request->cliente_id)->first();
            return view('despachos.listadosfactura',['lista'=>$tabla,'cliente'=>$clientes]);
           
   }  
    public function guardar(Request $request){
        
        $despacho= new \App\Despachos;
        $despacho->cliente_id=$request->cliente_id;
        $despacho->usuario_id=\Auth::user()->id;
        $despacho->save();

       $kardex= new \App\Kardex;
       $kardex->concepto="DESPACHO #".$despacho->id;
       $kardex->operacion=2;
       $kardex->localizacion=3;
       $kardex->save();


        for ($i=0 ; $i < count($request->productos)  ; $i++) { 
            $detalledespacho=new \App\DetalleDespachos;
            $detalledespacho->despacho_id=$despacho->id;
            $detalledespacho->detalle_id=$request->productos[$i]['id'];
            $detalledespacho->cantidad=$request->productos[$i]['cantidad'];
            $detalledespacho->save();
            $detallefactura= \App\DetalleFactura::find($request->productos[$i]['id']);
            $detallefactura->entregado=$detallefactura->entregado+$request->productos[$i]['cantidad'];
            $detallefactura->save();

              $detallekardex=new \App\DetalleKardex;
              $detallekardex->kardex_id=$kardex->id;
              $detallekardex->producto_id=$detallefactura->producto_id;
              $detallekardex->cantidad=-$request->productos[$i]['cantidad'];
              $detallekardex->precio=$detallefactura->precio;
              $detallekardex->save();

              $saldo=\App\Saldos::where('producto_id',$detallefactura->producto_id)->first();
              $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->where('localizacion_id',3)->first();
              $saldo_localizacion->salidas=$saldo_localizacion->salidas+$request->productos[$i]['cantidad'];
              $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
              $saldo_localizacion->saldos=$nuevosaldo;
              $saldo_localizacion->save();
        }
        return response()->json(["valid"=>true]);
   }

}
