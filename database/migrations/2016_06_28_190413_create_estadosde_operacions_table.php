<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstadosdeOperacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estadosde_operaciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('problema');
            $table->string('causa');
            $table->string('solucion');
            $table->integer('categoria_id')->unsigned();
            $table->integer('maquina_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('estadosde_operaciones');
    }
}
