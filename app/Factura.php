<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Factura extends Model
{
   use SoftDeletes;
   protected $table="factura";
   protected $fillable=["cliente_id","formapago","descuento","total","abonado","estado","usuario_id","fechaentrega"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
