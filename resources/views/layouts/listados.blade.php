<div class="modal-dialog">
<div class="container">
<div class="row">
<br>
    <div class="col-lg-8">
      <div class="panel panel-green">
            <div class="panel-heading">
              @yield('titulo')
            </div>
            <div class="panel-body">
        				 @yield('tabla')       		
            </div>    
            <div class="panel-body" align="right">
             <button type="button" class="btn btn-default" style='width: 100px' data-dismiss="modal">Cancelar</button>
            </div>
        </div>
     </div>    
</div>
</div>
</div>
 @yield('script')


