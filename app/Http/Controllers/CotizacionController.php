<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class CotizacionController extends Controller
{
  public function generarPDF(Request $requests){
    $datosbasico=\App\Datosbasicos::all();
    $cotizacion=\App\Cotizacion::where('id',$requests->cotizacion)->first();
    $detalle = DB::table('productos')->select('productos.codigo','productos.nombre','detallecotizacion.cantidad','detallecotizacion.precio')->join('detallecotizacion','detallecotizacion.producto_id','=','productos.id')->where('detallecotizacion.cotizacion_id',$cotizacion->id)->get();

     $pdf=\PDF::loadView('cotizacion.pdf',['factura'=>$requests->cotizacion,'datosbasico'=>$datosbasico,'datosfactura'=>$cotizacion,'detalle'=>$detalle]);
       return $pdf->stream('cotizacion.pdf');

  }  
  
  public function listado(Request $requests){
         $cotizacion=\App\Cotizacion::where('deleted_at',null)->orderBy('id','desc')->get() ;
         return view('cotizacion.listadocotizacion',['lista'=>$cotizacion]);
  }  
  public function guardar(Request $requests){
            
             $cotizacion= new \App\Cotizacion;
             $cotizacion->nombre=$requests->nombre;
             $cotizacion->observaciones=$requests->observaciones;
             $cotizacion->descuento=$requests->descuento;
             $cotizacion->total=$requests->total;
             $cotizacion->usuario_id=\Auth::user()->id;
             $cotizacion->save();


             for ($i=0; $i < count($requests->productos) ; $i++) {
                if ($requests->productos[$i]['estado']==0){ 
                  $detalle=new \App\DetalleCotizacion;  
                  $detalle->cotizacion_id=$cotizacion->id;
                  $detalle->producto_id=$requests->productos[$i]['producto_id'];
                  $detalle->cantidad=$requests->productos[$i]['cantidad'];
                  $detalle->precio=$requests->productos[$i]['precio'];
                  $detalle->save();
                }   
             }

             return response()->json(['valid'=>true]);
      }  

}


