$(function(){
   $("#frmdatosbasicos").on("submit", function(e){
   		var datos= $("#datos_id").val();
   		var url="";
   		var mensaje="";
   	 	var fields= $(this).serialize();
   		if (datos==0){
   			url="/grabardatosbasicos";
   			mensaje="Los datos a sido ingresados exitosamente";
   		}
   		else
   		{
   				url="/actualizardatosbasicos";
   				mensaje="";

   		}	

  		console.log(fields);
   		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){

			if (isConfirm){
					$.ajax({	
							url:url,
							type:'POST',
							data:fields,
							 dataType: "json",
							success: function(resp){	
								if (resp.valid !==undefined){
									$.notify("Los cambios se han sido realizados con exito","success");
								}
								else
								{	
									if(resp.nit!==undefined){
										$("#nit").focus();
										$.notify(resp.nit,"error");
										return false;
									}
									if(resp.razonsocial!==undefined){
										$("#razonsocial").focus();
										$.notify(resp.razonsocial,"error");
										return false;
									}
									if(resp.direccion!==undefined){
										$("#direccion").focus();
										$.notify(resp.direccion,"error");
										return false;
									}
									if(resp.telefonos!==undefined){
										$("#telefonos").focus();
										$.notify(resp.telefonos,"error");
										return false;
									}
									if(resp.celular!==undefined){
										$("#celular").focus();
										$.notify(resp.celular,"error");
										return false;
									}


								}		
							},
							error: function(jqXHR,estado,error){
								$.notify(error,"error");

							},
							complete:function(jqXHR,estado){
							
							}
					});



			}

		});


		
   		return false;        
    });
}); 
