@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
	<div class="row">
	 <div id="viewformato" class="col-lg-12">			
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Caja</h3>
	        </div>
	        <div class="panel-body">
	        	<div class="row">
	 				<div class="col-lg-12">
	 					<div class="panel panel-info">
			 					<div class="panel-heading">
			 					 <div class="row">
			 						<div class="col-lg-10">	
			           				 <h3 class="panel-title">Informe de Caja N°: {{$cierre->id}} <br> <br> Apertura: {{$cierre->fechainicio}}<br> <br> Cierre: {{$cierre->fechacierre}}</h3>
			           				 <input type='hidden' id='cierre' value={{$cierre->id}}>
			           				</div>
			           				<div class="col-lg-2">
			           				 
			           				 		<div class="form-group">
			           				 			<button type="button" id="btncierrecaja" class="btn btn-primary btn-block" >Cierre de caja</button>
			           				 			<button type="button" class="btn btn-success btn-block" id="btningresos" data-toggle="modal" data-target="#VentanaModal">Ingresos</button>
			           				 			<button type="button" class="btn btn-warning btn-block" id="btnegresos" data-toggle="modal" data-target="#VentanaModal">Egresos</button>
			           				 		</div>

			           				</div> 
			           			 </div> 	
			        			</div>
			 					<div class="panel-body">
			 					    <div class="row">
			 						 <div class="col-lg-12">	
								         <div class="table-responsive">
								       		<table id="tblclientes" class="table table-triped" >       
								       			<thead> 		
									    				 <tr>
								        					<th width="10%">
								        						Fecha
								        					</th>
								        					<th width="30%">
								        						Concepto
								        					</th>
								        					<th width="40%">
								        						Detalle
								        					</th>
								        					<th width="20%">
								        						Valor
								        					</th>
								        					
								        				</tr>
								        		</thead>
								        		<tbody>
								        				
								        					<?php
								        						$total=0;
								        						for ($i=0; $i < count($documentos); $i++) { 
								        							echo "<tr>";
								        							echo "<td>".$documentos[$i]->created_at."</td><td>".$documentos[$i]->nombre."</td><td>".$documentos[$i]->detalle."</td><td>$ ".number_format($documentos[$i]->valor)."</td>";
								        							echo "</tr>";
								        							$total+=$documentos[$i]->valor;
								        						}
								        					?>	
								        			</tbody>
								          	</table>
								          	<table class="table table-triped" >
								          	 		<tr>
								        					<th width="80%" style="text-align:right;">
								        						Total: 
								        					</th>
								        					<td width="30%">
								        						<?php
								        							echo "$ ".number_format($total);
								        						?>
								        					</td>
								        					<input type='hidden' id='total' value={{$total}}>
								        					
								        			</tr>
								        				
								          	</table>	
								        </div>
								    </div>
								 </div>
								</div>
					    </div>
				    </div>
				</div>        
	    </div>
	   {!!Form::close()!!} 
	 </div>
	
</div>

@endsection	
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({

    	 "language":	
            {
            	 "url": "/assets/js/pluginspanishtabla"
            }
    });
});	
	
</script>
{!!Html::script('assets/js/caja.js')!!}
@endsection


