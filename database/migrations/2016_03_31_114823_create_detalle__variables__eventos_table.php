<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalleVariablesEventosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle__variables__eventos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('evento_variable_id')->unsigned();
            $table->foreign('evento_variable_id')->references('id')->on('eventos__variables')->onDelete('cascade');
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
            $table->string('v_cadena');
        $table->bigInteger('v_numerio_entero');
        $table->double('v_numerio_decimal',15,2);
            $table->time('v_hora');
            $table->date('v_fecha');
            $table->dateTime('v_fechahora');
            $table->json('v_json');
            $table->json('v_imagen');
            $table->json('v_archivo');
            $table->json('v_lista');





        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detalle__variables__eventos');
    }
}
