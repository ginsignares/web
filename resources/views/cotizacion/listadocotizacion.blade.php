@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
	 <div id="viewformato" class="col-lg-12">			
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Listado de Cotizaciones</h3>
	        </div>
	        <div class="panel-body">
	        	
								         <div class="table-responsive">
								       		<table id="tblclientes" class="table table-triped" >      <thead>  		
									    				 <tr>
								        					<th>
								        						Numero
								        					</th>
								        					<th>
								        						Nombre
								        					</th>
								        					<th>
								        						Observaciones
								        					</th>
								        					<th width="10%" align="center">
								        						Ver
								        					</th>
								        				</tr>
								        			</thead>
								        			<tbody>	
								        					<?php
								        						
								        						for ($i=0; $i < count($lista); $i++) { 
								        							echo "<tr>";
								        							echo "<td> COT-".$lista[$i]->id."</td><td>".$lista[$i]->nombre."</td><td> ".$lista[$i]->observaciones."</td><td><label onclick=ver(".$lista[$i]->id.") class='btn btn-success btn-block'><i class='fa fa-check-circle'> Ver</label></td>";
								        							echo "</tr>";
								        						}
								        					?>
								        			</tbody>	
								          	</table>
								        </div>
								    </div>
				</div>        
	    </div>
	   {!!Form::close()!!} 
	 </div>
	
</div>
</div>

@endsection	
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({

    	 "language":	
            {
            	 "url": "/assets/js/pluginspanishtabla"
            }
    });
});	

</script>
	{!!Html::script('assets/js/cotizaciones.js')!!}
@endsection

