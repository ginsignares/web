@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
	 <div id="viewformato" class="col-lg-12">			
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Analisis de Cartera</h3>
	        </div>
	        <div class="panel-body">
	        	<div class="row">
	 				<div   id="listadofacturas"  class="col-lg-12">
	 					<div class="panel panel-info">
			 					<div class="panel-heading">
			           				 <h3 class="panel-title">Clientes</h3>
			        			</div>
			 					<div class="panel-body">
								         <div class="table-responsive">
								       		<table id="tblclientes" class="table table-triped" >      <thead>  		
									    				 <tr>
									    				    <th>
								        						Nit
								        					</th>
								        					<th>
								        						Razón social
								        					</th>
								        					<th>
								        						Teléfonos
								        					</th>
								        					<th>
								        						Email
								        					</th>
								        					<th>
								        						Saldo			
								        					</th>
								        					<th>
								        						Seleccionar			
								        					</th>
								        					
								        				</tr>
								        		</thead>
								        		<tbody>	
								        					<?php
								        						$totalcartera=0;
								        						for ($i=0; $i < count($lista); $i++) { 
								        							echo "<tr>";
								        							echo "<td>".$lista[$i]['nit']."</td>";
								        						echo "<td>".$lista[$i]['razonsocial']."</td>";
								        						echo "<td>".$lista[$i]['celular']." ".$lista[$i]['telefono']."</td>";
								        						echo "<td>".$lista[$i]['email']."</td>";
								        						echo "<td>".number_format($lista[$i]['saldo'])."</td>";
								        						echo "<td><label onclick=consultar(".$lista[$i]['cliente_id'].") class='btn btn-info btn-block'><i class='fa fa-check-circle'> Seleccionar</label></td>";
								        							echo "</tr>";
								        							$totalcartera+=$lista[$i]['saldo'];	
								        						}
								        							
								        					?>
								        		</tbody>	
								          	</table>
								          	<table style="align:right">
								          			<?php
								          	  					echo "<tr>";
								        						echo "<td colspan=4><h1>Total: </h1> </td>";
								        						echo "<td style='color:#FF0000'><h1>".number_format($totalcartera)."</h1></td>";

								        						echo "</tr>";
								        			?>
								          	</table>	
								        </div>
								    </div>
					    </div>
				    </div>
				</div>        
	    </div>
	   {!!Form::close()!!} 
	 </div>
	
</div>
</div>
@endsection
@section('script')
{!!Html::script('assets/js/cartera.js')!!}
<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({

    	 "language":	
            {
            	 "url": "/assets/js/pluginspanishtabla"
            }
    });
});	

	
</script>
@endsection