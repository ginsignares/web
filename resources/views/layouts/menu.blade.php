<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/bootstrap/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="/sw/sweetalert.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/www/bower_components/datatables/media/css/jquery.dataTables.min.css">
   
   

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="/">RAPICOPIAS EM</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
    
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i><b class="caret"></b></a>
                    <ul class="dropdown-menu">  
                        <li>
                            <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->

            <div class="collapse navbar-collapse navbar-ex1-collapse">
              
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a data-toggle="collapse" data-target="#parametros"><i class="fa fa-gears"></i> Configuración<i class="fa fa-fw fa-caret-down"></i></a>

                         <ul id="parametros" class="collapse">
                            <li>
                                <a href="/parametros/datosbasicos">Datos básicos</a>
                            </li>
                            <li>
                                <a href="/parametros/clientes">Clientes</a>
                            </li>
                            <li>
                                <a href="/parametros/productos">Productos</a>
                            </li>
                             <li>
                                <a href="/parametros/proveedores">Proveedores</a>
                            </li>

                        </ul>
                    </li>
                     <li>

                     <a data-toggle="collapse" data-target="#cotizacion"><i class="fa fa-fw fa-list-alt"></i> Cotización<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="cotizacion" class="collapse">
                            <li>
                                <a href="/menu/cotizacion">Crear</a>
                            </li>
                            <li>
                                <a href="/listadocotizacion">Consultar</a>
                            </li>
                         
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="collapse" data-target="#factura"><i class="fa fa-fw fa-list-alt"></i> Facturas<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="factura" class="collapse">
                            <li>
                                <a href="/menu/facturas">Crear</a>
                            </li>
                            <li>
                                <a href="/listadopedido">Consultar</a>
                            </li>
                         
                        </ul>
                    </li>
                   
                    <li>
                        <a data-toggle="collapse" data-target="#pedidos"><i class="fa fa-fw fa-list-alt"></i> Pedidos<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="pedidos" class="collapse">
                            <li>
                                <a href="/listadofacturas">Por productos</a>
                            </li>
                            <li>
                                <a href="/listadofacturasporfechapedido">Por fecha de entrega</a>
                            </li>
                         
                        </ul>

                    </li>
                    <li>
                        <a href="/menu/despachos"><i class="fa fa-fw fa-cube"></i> Despachos</a>
                    </li>
                     <li>
                        <a href="/menu/ventas"><i class="fa fa-fw fa-shopping-cart"></i> Ventas</a>
                    </li>
                    <li>
                        <a href="/menu/cartera"><i class="fa fa-list-ol fa-3x" style="font-size:15px"></i> Cartera</a>
                    </li>
                    <li>
                        <a href="/menu/caja"><i class="fa fa-fw fa-money"></i> Caja</a>
                    </li>
                    <li>
                        <a data-toggle="collapse" data-target="#inventario"><i class="fa fa-fw fa-list-alt" ></i> Inventario<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="inventario" class="collapse">
                            <li>
                                <a href="/menu/saldosiniciales">Saldos Iniciales</a>
                            </li>
                            <li>
                                <a href="/menu/compras">Compras</a>
                            </li>
                            <li>
                                <a href="/menu/requisiciones">Requisiciones</a>
                            </li>

                        </ul>
                    </li>
                    <li>
                        <a  data-toggle="collapse" data-target="#informes"><i class="fa fa-fw fa-print" ></i> Informes <i class="fa fa-fw fa-caret-down"></i></a>
                          <ul id="informes" class="collapse">
                            <li>
                                <a href="/informeexistencias/1">Existencias</a>
                            </li>
                            <li>
                                <a href="/analisiscartera">Análisis de cartera</a>
                            </li>
                            <li>
                                <a href="/historicodeventas">Histórico de ventas </a>
                            </li>
                             <li>
                                <a href="/historicodepedidos">Historico de pedidos</a>
                            </li>
                            <li>
                                <a href="/historicodecaja">Historico de caja</a>
                            </li>
                            <li>
                                <a href="#">Auditoria de sistema</a>
                            </li>
                        </ul>


                    </li>
                     <li>
                        <a href="/logout"><i class="fa fa-fw fa-power-off"></i> Logout</a>
                    </li>
                    
                </ul>
                
                
               
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
             <div class="container-fluid">
                @yield('contenido')
             </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
  
    <script src="/bootstrap/js/jquery.js"></script>  
    <script src="/www/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    
    
    <script src="/bootstrap/js/notify.min.js"></script>
    <script src="/sw/sweetalert.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    
    @yield('script')


</body>

</html>
        