
var productos= new Array();
var descuento=0;
var abono=0;
var subtotal=0;
var total=0;

var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft  +splitRight;
 },
 new:function(num, simbol){
  this.simbol = simbol ||'';
  return this.formatear(num);
 }
}
function grabaritem(id){

	var Q=$("#Q"+id).val();
	var Qp=$("#Qp"+id).val();
	if (Q>Qp || Q<0){
				$.notify("La cantidad ingresada no es valida","error");
				return;
			}
	
		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
			
			if(isConfirm)
			{
				$.ajax({	
						url:"/actulizarproducto",
						type:'post',
						data:{detalle_id:id,cantidad:Q},
						dataType:"html",
						success: function(resp){
								    
										$.notify("Los cambios se han sido realizados con exito","success");
										$(location).attr('href','/listadofacturas');
									
						},
						error: function(jqXHR,estado,error){
							$.notify(error,"error");

						},
						complete:function(jqXHR,estado){
						
						}
				});

			}	


		});
}


function ver(id){
	 window.open('/generarPDF/'+id, '_blank');
}

function consultar(id){
			
		$.ajax({	
			url:"/formato",
			type:'get',
			data:{cliente_id:id,formato_id:0},
			dataType:"html",
			success: function(resp){
				 $("#viewformato").html(resp)
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
} 
$("#btnguardar").click(function(){
	var cliente=$("#cliente_id").val();
	var fechaentrega=$("#fechaentrega").val();
	var abono=Number($("#abono").val());
	var descuento=Number($("#descuento").val());


	
	if (productos.length==0){
		$.notify("El sistema no encuentra productos asociados a la factura","error");
		return;
	}
	if (abono==0 || abono<0 || isNaN(abono)==true){
		$.notify("El sistema no puede generar la factura por que no encuentra un abono","error");
		return;
	}
	if (abono>total){
		$.notify("El abono supera el saldo de la factura","error");
		return;

	}

	swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
				if(isConfirm){
					$.ajax({	
						url:"/guardarpedido",
						type:'post',
						data:{cliente_id:cliente,fechaentrega:fechaentrega,productos:productos,abono:abono,valor:total,descuento:descuento},
						dataType:"json",
						success: function(resp){
							if (resp.valid !==undefined){
								$.notify("Los cambios se han sido realizados con exito","success");
								 window.open('/generarPDF/'+resp.factura, '_blank');
								 $(location).attr('href','/menu/facturas');
							}
							else
								$.notify(resp.mensaje,"error");					
						},
						error: function(jqXHR,estado,error){
							$.notify(error,"error");

						},
						complete:function(jqXHR,estado){
						
						}
					});
				}	
		});

});


$("#btnagregar").click(function(){
		$.ajax({	
			url:"/formatoproductos",
			type:'get',
			data:{formato_id:0},
			dataType:"html",
			success: function(resp){
				 $("#VentanaModal").html("");
				 $("#VentanaModal").html(resp);
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
});



$("#btncancelar").click(function(){
	$(location).attr('href','/menu/facturas');
});


function cargarproductos(){

	$("#cpdetallepedido").html("");
	var totalproductos=productos.length;
	var posicion=1;
    total=0;
	subtotal=0;

	for (var i = 0; i < totalproductos; i++) {
	   if (productos[i].estado=="0"){	
		total=productos[i].cantidad*productos[i].precio;
		$("#cpdetallepedido").append("<tr><td>"+posicion+"</td><td width='60%'>"+productos[i].nombre+"</td><td>"+productos[i].cantidad+"</td><td>$ "+productos[i].precio+"</td><td>$ "+total+"</td><td><label onclick=borrar("+i+") class='btn btn-danger btn-block'><i class='fa fa-times-circle'> Quitar</label></td></tr>");
		posicion++;
		subtotal+=total;
	   }	

	}

	total = subtotal-descuento;
	
	$("#subtotal").val(formatNumber.new(subtotal,"$ "));
    $("#total").val(formatNumber.new(total,"$ "));
	
}
function borrar(id){
	productos[id].estado="1";
	cargarproductos();
}
$("#descuento").change(function(){
	var Q= Number($("#descuento").val());

	if ((isNaN(Q)==true) | (Q<0) | (Q>subtotal)){
			$("#descuento").focus();
			$("#descuento").val('');
			$("#subtotal").val(formatNumber.new(subtotal,"$ "));
			$("#total").val(formatNumber.new(subtotal,"$ "));
			$("#saldo").val(formatNumber.new(saldo,"$ "));
			$.notify("La cantidad ingresada no es valida","error");
			return;
	}
	descuento=Q;
	total= subtotal-descuento;
	$("#subtotal").val(formatNumber.new(subtotal,"$ "));
    $("#total").val(formatNumber.new(total,"$ "));
	
});


