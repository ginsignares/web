<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleKardex extends Model
{
   protected $table="detallekardex";
   protected $fillable=["kardex_id","producto_id","cantidad","precio"];
}
