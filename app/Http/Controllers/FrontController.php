<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use DB;
class FrontController extends Controller
{
    	 public function __construct(){
            $this->middleware('auth');
          }

          public function saldos(){
   
            $tabla = DB::table('productos')->select()->where('deleted_at',null)->get(); 

            if ($tabla!=null){

                $r=array();    
                for ($i=0; $i < count($tabla); $i++) { 
                   array_push($r,['codigo'=>$tabla[$i]->fabricado]);

                    $saldos= new \App\Saldos;
                    $saldos->producto_id=$tabla[$i]->id;
                    $saldos->save();

                    $saldos_localizacion= new \App\SaldosLocalizacion;
                    $saldos_localizacion->saldo_id=$saldos->id;
                    $saldos_localizacion->localizacion_id=1;
                    $saldos_localizacion->inv_inicial=0;
                    $saldos_localizacion->save();

                    if ($tabla[$i]->fabricado=="Si"){
                        $saldos_localizacion= new \App\SaldosLocalizacion;
                        $saldos_localizacion->saldo_id=$saldos->id;
                        $saldos_localizacion->localizacion_id=3;
                        $saldos_localizacion->inv_inicial=0;
                        $saldos_localizacion->save();
                    }


                }
            }
                  return $r;


          
             }      

    	public function datosbasico(){
   
            $tabla = DB::table('Datosbasicos')->select()->where('deleted_at',null)->first(); 

            if ($tabla!=null)
    		  return view('configuracion.datosbasicos',["datos"=>$tabla,"opcion"=>1]);
    	    else
               return view('configuracion.datosbasicos',["datos"=>$tabla,"opcion"=>0]);
        } 
    	public function clientes(Request $requests){
            $tabla = DB::table('clientes')->select()->where('id',$requests->cliente_id)->where('deleted_at',null)->first(); 
            if ($tabla!=null)
                return response()->json($tabla);
            else
                return view('configuracion.clientes',['datos'=>$tabla,"opcion"=>0]);
    		
    	} 
    	public function productos(Request $requests){
            if ($requests->producto_id<>null){
            $tabla = DB::table('productos')->select()->where('id',$requests->producto_id)->where('deleted_at',null)->first(); 
            if ($tabla!=null)
                return response()->json($tabla);
            else
                return view('configuracion.productos');
            }
            else
                return view('configuracion.productos');
    		
    	} 
        public function pedidos(Request $requests){
             $tabla = DB::table('clientes')->select()->where('deleted_at',null)->get(); 

             return view('pedidos.pedidos',['lista'=>$tabla]);
         
        }
        public function saldosiniciales(Request $requests){
            $tabla=  DB::table('productos')->select('productos.id as producto_id','productos.nombre','saldos.id as saldo_id','saldos_localizaciones.id as saldo_l_id')->join('saldos','saldos.producto_id','=','productos.id')->join('saldos_localizaciones','saldos_localizaciones.saldo_id','=','saldos.id')->where('saldos_localizaciones.inv_inicial','=',0)->where('saldos_localizaciones.localizacion_id',1)->get();

             return view('inventario.saldos',['lista'=>$tabla]);
         
        }
         public function compras(Request $requests){
             $tabla = DB::table('proveedores')->select()->where('deleted_at',null)->get(); 

             return view('inventario.compras',['lista'=>$tabla]);
         
        }
        public function proveedores(Request $requests){
            $tabla = DB::table('proveedores')->select()->where('id',$requests->proveedor_id)->where('deleted_at',null)->first(); 
            if ($tabla!=null)
                return response()->json($tabla);
            else
                return view('configuracion.proveedores',['datos'=>$tabla,"opcion"=>0]);
            
        } 

          public function ventas(Request $requests){
             $tabla = DB::table('clientes')->select()->where('deleted_at',null)->get(); 

             return view('ventas.ventas',['lista'=>$tabla]);
         
        }

       public function cotizacion(Request $requests){

             return view('cotizacion.formato');
         
        }

        public function analisiscartera(Request $request){
            $clientes=\App\Clientes::all();
            $cartera=array();    
            if ($clientes!=null){
                for ($i=0; $i<count($clientes); $i++) { 
                    $facturas=\App\Factura::where('cliente_id',$clientes[$i]->id)->where('formapago',1)->get();
                    $saldo=0;
                    if ($facturas!=null)
                    {
                        for ($j=0; $j <count($facturas); $j++) { 
                                $saldomora=$facturas[$j]->total-$facturas[$j]->abonado;
                                if ($saldomora>0)
                                   $saldo+=$saldomora;    
                        }
                    }    

                    if ($saldo>0)
                        array_push($cartera,['cliente_id'=>$clientes[$i]->id,'nit'=>$clientes[$i]->nit,'razonsocial'=>$clientes[$i]->razonsocial,'telefono'=>$clientes[$i]->telefono,'celular'=>$clientes[$i]->celular,'email'=>$clientes[$i]->email,'saldo'=>$saldo]);
                }
            }    

            return view('cartera.analisis',['lista'=>$cartera]);
        }

        public function historicodeventas(){

            return view('informes.historicodeventas',['lista'=>array()]);
        } 

        
         public function historicodepedidos(){

            return view('informes.historicopedidos',['lista'=>array()]);
        }
        public function historicodecaja(){

            return view('informes.historicodecaja',['lista'=>array()]);
        }     
     

        public function listadofacturafecha(Request $request){
            $tabla = DB::table('factura')->select('clientes.id as cliente_id','clientes.nit','clientes.razonsocial','factura.id as factura_id','factura.created_at','factura.formapago','factura.descuento','factura.total','factura.abonado')->join('clientes','clientes.id','=','factura.cliente_id')->wherebetween('factura.created_at',[$request->fi." 00:00:00",$request->ff." 23:59:59"])->get();
            return $tabla; 
        }

        public function listadopedidosfecha(Request $request){
            $tabla = DB::table('kardex')->select('kardex.concepto','kardex.created_at','kardex.created_at','detallekardex.cantidad','productos.nombre')->join('detallekardex','detallekardex.kardex_id','=','kardex.id')->join('productos','productos.id','=','detallekardex.producto_id')->where('kardex.operacion',1)->where('kardex.localizacion','<>',1)->wherebetween('kardex.created_at',[$request->fi." 00:00:00",$request->ff." 23:59:59"])->get();
            return $tabla; 
        }

          public function listadocajafecha(Request $request){
            $tabla = DB::table('conceptos')->select('conceptos.nombre','documentos.detalle','documentos.valor','documentos.created_at')->join('documentos','documentos.concepto_id','=','conceptos.id')->where('conceptos.sistema',0)->where('conceptos.tipo',1)->wherebetween('documentos.created_at',[$request->fi." 00:00:00",$request->ff." 23:59:59"])->get();
            return $tabla; 
        }
}
