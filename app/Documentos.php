<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Documentos extends Model
{
   use SoftDeletes;
   protected $table="documentos";
   protected $fillable=["concepto_id","cierre_id","detalle","valor","usuario_id"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
