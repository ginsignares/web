<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaldosLocalizacion extends Model
{
   protected $table="saldos_localizaciones";
   protected $fillable=["saldo_id","localizacion_id","inv_inicial","entradas","salidas","saldos"];
}
