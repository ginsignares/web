<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/bootstrap/css/sb-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/bootstrap/css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/bootstrap/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="/sw/sweetalert.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="/www/bower_components/datatables/media/css/jquery.dataTables.min.css">
   
   

</head>

<body>

 
                <div class="modal-dialog">
                    <div class="container">
                        @yield('contenido')
                    </div>
                </div>

 <script src="/bootstrap/js/jquery.js"></script>  
    <script src="/www/bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    
    
    <script src="/bootstrap/js/notify.min.js"></script>
    <script src="/sw/sweetalert.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    
    @yield('script')


</body>

</html>
