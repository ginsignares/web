<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCompras extends Model
{
    protected $table="detallecompra";
    protected $fillable=["compra_id","producto_id"];
    public $timestamps = false;
}
