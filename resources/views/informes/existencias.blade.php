@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
     <div id="viewformato" class="col-lg-12">           
        <div class="panel panel-green">
            <div class="panel-heading">
                <h3 class="panel-title">Listados de productos</h3>
            </div>
            <div class="panel-body">
                          <div class="row">
                            <div class="col-lg-4"> 
                                <div class="form-group">
                                   <label>Ubicacion
                                              <select id="ubicaciones" class="form-control">
                                                @if ($localizacion==1)
                                                  <option value=1>Principal</option>
                                                  <option value=3>Pedidos</option>
                                                @else

                                                  <option value=3>Pedidos</option>
                                                  <option value=1>Principal</option>
                                                @endif
                                              </select>
                                         </label> <button type='button' class="btn btn-primary" onclick="consultar()">Consultar</button>
                                  </div>  
                                </div>
                              </div>  
                           
                         
                          <div class="row">
                            <div class="col-lg-12"> 
                              <div class="panel panel-info">
                                    <div class="panel-heading">
                                         <h3 class="panel-title">Saldos</h3>
                                    </div>
                                    <div class="panel-body">
                                         <div class="table-responsive">
                                            <table id="dataTables-tblclientes" name="dataTables-tblclientes" class="table table-triped" >      
                                            <thead>          
                                                         <tr>
                                                            <th>
                                                                Producto
                                                            </th>
                                                            <th>
                                                                Existencia
                                                            </th>
                                                        </tr>
                                            </thead>
                                                        <tbody>
                                                            <?php
                                                                for ($i=0; $i < count($lista); $i++) {

                                                                  echo "<tr><td>".$lista[$i]->nombre."</td><td>".$lista[$i]->saldos."</td></tr>";
                                                                }
                                                            ?>  
                                                        </tbody>
                                            </table>
                                        </div>
                                      </div>
                                </div>
                                    </div>
                              </div>
                </div>        
        </div>
     
     </div>
    
</div>

@endsection 
@section('script')
<script type="text/javascript">
$(document).ready(function() {
$('#dataTables-tblclientes').dataTable( {
            "language": 
            {
               "url": "/assets/js/pluginspanishtabla"
            }
        } );

  
} );
  
function consultar(){
    var bodega=$("#ubicaciones").val();
    $(location).attr('href','/informeexistencias/'+bodega);

} 
</script>
@endsection 


