<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class CarteraController extends Controller
{
     public function cartera(){
        $clientes=\App\Clientes::where('deleted_at',null)->get();
        return view('cartera.cartera',['lista'=>$clientes]);
   }
     public function listado(Request $request){
        $tabla = DB::table('factura')->select('factura.id as factura_id','factura.created_at','factura.cliente_id','clientes.razonsocial','factura.abonado','factura.total','factura.descuento')->join('clientes','clientes.id','=','factura.cliente_id')->where('factura.cliente_id',$request->cliente_id)->where('factura.deleted_at',null)->where('formapago',1)->get(); 
        //return $tabla;
     
        $clientes=\App\Clientes::where('id',$request->cliente_id)->first();

            return view('cartera.listadofactura',['lista'=>$tabla,'cliente'=>$clientes]);
           
   }

      public function guardar(Request $request){


        for ($i=0 ; $i < count($request->productos)  ; $i++) { 
        	$cierre= DB::table('cierres')->select()->OrderBy('id','desc')->first();

            $factura=\App\Factura::find($request->productos[$i]['id']);
            $factura->abonado+=$request->productos[$i]['cantidad'];
            if ($factura->total==$factura->abonado)
            	$factura->estado="Pagado";
            $factura->save();
            $documento=new \App\Documentos;
            $documento->concepto_id=2;
            $documento->cierre_id=$cierre->id;
            $documento->detalle="FAC # ".$factura->id;
            $documento->valor=$request->productos[$i]['cantidad'];
            $documento->usuario_id=\Auth::user()->id;
            $documento->save();
        }
        return response()->json(["valid"=>true]);
   }
}
