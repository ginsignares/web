var productos= new Array();
var subtotal=0;
var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft  +splitRight;
 },
 new:function(num, simbol){
  this.simbol = simbol ||'';
  return this.formatear(num);
 }
}
function grabaritem(id){
	var Q=$("#Q"+id).val();
	var Qp=$("#Qp"+id).val();
	if (Q>Qp || Q<0){
		$.notify("La cantidad ingresada no es valida","error");
		return;
	}
	
		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
			
			if (isConfirm)
			{
				$.ajax({	
						url:"/actulizarproducto",
						type:'post',
						data:{detalle_id:id,cantidad:Q},
						dataType:"html",
						success: function(resp){
								    
										$.notify("Los cambios se han sido realizados con exito","success");
										$(location).attr('href','/controlpedido');
									
						},
						error: function(jqXHR,estado,error){
							$.notify(error,"error");

						},
						complete:function(jqXHR,estado){
						
						}
				});

			}	


		});
}

$("#btnguardar").click(function(){

	var nombrecompleto=$("#nombre").val();
	var observaciones=$("observaciones").val();
	if(productos.length==0){
		$.notify("La requisición no contiene productos para ser creada","error");		
		return;
	}

	swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
			
			if (isConfirm)
			{
			
				
				$.ajax({	
					url:'/menu/guardarrequisicion',
					type:'POST',
					data:{nombre:nombrecompleto,productos:productos,observaciones:observaciones},
					success: function(resp){
						if (resp.valid !==undefined){
							$.notify("Los cambios se han sido realizados con exito","success");
							$(location).attr('href','/menu/requisiciones');
						}	
					},
					error: function(jqXHR,estado,error){
						$.notify(error,"error");

					},
					complete:function(jqXHR,estado){
					
					}
			
				});
			}	
		});

});


$("#btnagregar").click(function(){
		$.ajax({	
			url:"/menu/productosrequisicion",
			type:'get',
			data:{formato_id:0},
			dataType:"html",
			success: function(resp){
				 $("#VentanaModal").html("");
				 $("#VentanaModal").html(resp);
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
});

$("#btncancelar").click(function(){
	$(location).attr('href','/menu/requisiciones');
});

function cargarproductos(){

	$("#cpdetallepedido").html("");
	var totalproductos=productos.length;
	var posicion=1;
	var total=0;
	subtotal=0;

	for (var i = 0; i < totalproductos; i++) {
	   if (productos[i].estado=="0"){	
		total=productos[i].cantidad*productos[i].precio;
		$("#cpdetallepedido").append("<tr><td>"+posicion+"</td><td width='60%'>"+productos[i].nombre+"</td><td>"+productos[i].cantidad+"</td><td><label onclick=borrar("+i+") class='btn btn-danger btn-block'><i class='fa fa-times-circle'> Quitar</label></td></tr>");
		posicion++;
		subtotal+=total;
	   }	

	}
	

}
function borrar(id){
	productos[id].estado="1";
	cargarproductos();
}