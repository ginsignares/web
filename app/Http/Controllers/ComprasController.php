<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;


class ComprasController extends Controller
{
    public function formato(Request $requests){
        if ($requests['formato_id']==0){
          $tabla = DB::table('proveedores')->select()->where('id',$requests->proveedor_id)->where('deleted_at',null)->first(); 
             return view('inventario.formato',['datos'=>$tabla,'fecha'=>date("Y-m-d")]);
        }
       
    }   
    public function productos(Request $requests){
      
          $tabla = DB::table('productos')->select()->where('fabricado','No')->where('deleted_at',null)->get(); 
          return view('inventario.listadoproducto',['lista'=>$tabla]);
    }   
    public function formatoproductos(Request $requests){
             $tabla = DB::table('productos')->select('productos.id as producto_id','productos.nombre','productos.precio','saldos_localizaciones.saldos')->join('saldos','saldos.producto_id','=','productos.id')->join('saldos_localizaciones','saldos_localizaciones.saldo_id','=','saldos.id')->where('productos.deleted_at',null)->get(); 
             return view('ventas.listadoproductos',['lista'=>$tabla]);
    }    

      public function guardar(Request $requests){
            
             $compras= new \App\Compras;
             $compras->proveedor_id=$requests->proveedor_id;
             $compras->factura=$requests->factura;
             $compras->fechacompra=$requests->fechacompra;
             $compras->observaciones=$requests->observaciones;
             $compras->usuario_id=\Auth::user()->id;
             $compras->save();


          
             $kardex= new \App\Kardex;
             $kardex->concepto="COMPRA FAC #".$requests->factura;
             $kardex->operacion=1;
             $kardex->localizacion=1;
             $kardex->save();

             for ($i=0; $i < count($requests->productos) ; $i++) {
                if ($requests->productos[$i]['estado']==0){ 
                  $detalle=new \App\DetalleCompras;  
                  $detalle->compra_id=$compras->id;
                  $detalle->producto_id=$requests->productos[$i]['producto_id'];
                  $detalle->cantidad=$requests->productos[$i]['cantidad'];
                  $detalle->precio=$requests->productos[$i]['precio'];
                  $detalle->save();

                  $detallekardex=new \App\DetalleKardex;
                  $detallekardex->kardex_id=$kardex->id;
                  $detallekardex->producto_id=$requests->productos[$i]['producto_id'];
                  $detallekardex->cantidad=-$requests->productos[$i]['cantidad'];
                  $detallekardex->precio=$requests->productos[$i]['precio'];
                  $detallekardex->save();

                  $saldo=\App\Saldos::where('producto_id',$requests->productos[$i]['producto_id'])->first();
                  $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->first();
                  $saldo_localizacion->entradas=$saldo_localizacion->entradas+$requests->productos[$i]['cantidad'];
                  $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
                  $saldo_localizacion->saldos=$nuevosaldo;
                  $saldo_localizacion->save();

                }   
             }

             return response()->json(['valid'=>true]);
      }  
}
