var productos= new Array();
var subtotal=0;
function consultar(id){
		$.ajax({	
			url:"/obtenerproductosfacturar",
			type:'get',
			data:{cliente_id:id,formato_id:0},
			dataType:"html",
			success: function(resp){
				 $("#viewformato").html(resp)
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
} 
function descuento(){
			
	var Q= $("#descuento").val();

	if ((isNaN(Q)==true) | (Q<0) | (Q==0 ) | (Q>subtotal)){
			$("#descuento").focus();
			$("#descuento").val('');
			$("#subtotal").val(formatNumber.new(subtotal,"$ "));
			$("#total").val(formatNumber.new(subtotal,"$ "));
			$.notify("La cantidad ingresada no es valida","error");
			return;
	}
	var neto=subtotal-Q;
	$("#total").val(formatNumber.new(neto,"$ "));
}

function agregar(id){
	var Q=Number($("#Q"+id).val());	
	var QV=Number($("#QV"+id).val());

	if ((isNaN(Q)==true) | (Q<0) | (Q>QV)){
		$.notify("El valor ingresado no es valido");
		return false;
	}
	var sw=-1;
	for (var i = 0; i <productos.length; i++) {
		if (Number(productos[i].detalle_id)==Number(id))
		{
			sw=i;
		}		
	}
	console.log(sw);
	if (sw!=-1)
		productos[sw].cantidad=Q;
	else
	{
		if (Q==0){
			$.notify("El valor ingresado no es valido");
			return false;
		}
		$.ajax({	
			url:"/obtenerdetallepedido",
			type:'post',
			data:{detalle_id:id},
			dataType:"json",
			success: function(resp){
				 productos.push({detalle_id:resp[0].id,pedido_id:resp[0].pedido_id,producto_id:resp[0].producto_id,precio:resp[0].precio,cantidad:Q});
				 console.log(productos);
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
				calcularsubtotal();
			}
		});	

		
	}	
		
	$("#R"+id).val(Q);

	console.log(productos);

} 
var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft  +splitRight;
 },
 new:function(num, simbol){
  this.simbol = simbol ||'';
  return this.formatear(num);
 }
}
function calcularsubtotal(){

	for (var i = 0; i < productos.length; i++) {
		subtotal+=productos[i].cantidad*productos[i].precio;
	}

	$("#subtotal").val(formatNumber.new(subtotal,"$ "));
	$("#total").val(formatNumber.new(subtotal,"$ "));
}
function generardespacho(){
	if (productos.length==0){
		$.notify("El sistema no encuentra productos registrados");
		return false;
	}
	else
	{
		var sw=-1;
		for (var i = 0; i <productos.length; i++) {
			if (Number(productos[i].cantidad)>0)
			{
				sw=i;
			}		
		}

		if (sw==-1){
				$.notify("El sistema no encuentra productos registrados");
			return false;
		}
	}	

	swal({
	title:"!Advertencia¡",
	text:"¿Esta seguro de realizar los cambios en el sistema?",
	showCancelButton:true,
	type:"warning",
	confirmButtonColor:'#e92c43',
	cancelButtonColor:'#e92c43',
	confirmButtonText:"Aceptar"
	}, function (isConfirm){

		var cliente=$("#cliente_id").val();
		var formapago=$("#formapago").val();
		var descuento=$("#descuento").val();
		var total=$("#total").val();
		$.ajax({	
				url:"/grabarfacturapedido",
				type:'POST',
				data:{cliente_id:cliente,productos:productos,formapago:formapago,descuento:descuento,valor:subtotal},
				dataType:"json",
				success: function(resp){
					$.notify("Los cambios se han sido realizados con exito","success");
					$(location).attr('href','/facturarpedido');
				},
				error: function(jqXHR,estado,error){
					$.notify(error,"error");

				},
				complete:function(jqXHR,estado){
				
				}
		});
	});		

}