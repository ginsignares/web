<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FacturaPedido extends Model
{
   protected $table="factura_pedido";
   protected $fillable=["factura_id","pedido_id"];

   public $timestamps = false;
}
