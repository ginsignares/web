var productos= new Array();

function consultar(id){
		$.ajax({	
			url:"/menu/listadodespacho",
			type:'get',
			data:{cliente_id:id},
			dataType:"html",
			success: function(resp){

				$("#listadofacturas").html(resp)
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
} 

function agregar(id){
	var Q=Number($("#Q"+id).val());	
	var QV=Number($("#QV"+id).val());

	if ((isNaN(Q)==true) | (Q<0) | (Q>QV)){
		$.notify("El valor ingresado no es valido");
		return false;
	}
	var sw=-1;
	for (var i = 0; i <productos.length; i++) {
		if (Number(productos[i].id)==Number(id))
		{
			sw=i;
		}		
	}
	console.log(sw);
	if (sw!=-1)
		productos[sw].cantidad=Q;
	else
	{
		if (Q==0){
			$.notify("El valor ingresado no es valido");
			return false;
		}
		productos.push({id:id,cantidad:Q});
	}	
		

	console.log(productos);

} 
function generardespacho(){
	if (productos.length==0){
		$.notify("El sistema no encuentra productos agregados a este despacho");
		return false;
	}
	else
	{
		var sw=-1;
		for (var i = 0; i <productos.length; i++) {
			if (Number(productos[i].cantidad)>0)
			{
				sw=i;
			}		
		}

		if (sw==-1){
			$.notify("El sistema no encuentra productos agregados a este despacho");
			return false;
		}
	}	

	swal({
	title:"!Advertencia¡",
	text:"¿Esta seguro de realizar los cambios en el sistema?",
	showCancelButton:true,
	type:"warning",
	confirmButtonColor:'#e92c43',
	cancelButtonColor:'#e92c43',
	confirmButtonText:"Aceptar"
	}, function (isConfirm){
	  if (isConfirm) {	
		var cliente=$("#cliente_id").val();
		$.ajax({	
				url:"/generardespacho",
				type:'POST',
				data:{cliente_id:cliente,productos:productos},
				dataType:"json",
				success: function(resp){
					$.notify("Los cambios se han sido realizados con exito","success");
					$(location).attr('href','/menu/despachos');
				},
				error: function(jqXHR,estado,error){
					$.notify(error,"error");

				},
				complete:function(jqXHR,estado){
				
				}
		});
	   }	
	});		

}