<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombreitem', 100);
            $table->integer('variable_id')->unsigned();
            $table->foreign('variable_id')->references('id')->on('variables')->onDelete('cascade');
            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('tipo_items')->onDelete('cascade');
            $table->integer('cantidadcaracteres')->unsigned();
            $table->double('cantidadfija','15','2')->unsigned();
            $table->integer('formatofecha')->unsigned();
            $table->integer('tipoarchivo')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('items');
    }
}
