$(document).ready(function() {
	var formulario=$("#txtformulario").val();

	$.post("/consultaeventosxformulario",{formulario:formulario},function(data){

		var totalregitros=data.length;
		var tabla=$("#dataTables-trazabilidad tbody");
		for (var i = 0; i < totalregitros; i++) {
			tabla.append("<tr onclick=mostrartrazabilidad("+data[i][0].evento_id+","+data[i][0].sede_id+","+data[i][0].formulario_id+","+data[i][0].item_id+",'"+data[i][0].valor+"')><td>"+data[i][0].fecha+"</td><td>"+data[i][0].formulario+"</td><td>"+data[i][0].nombreitem+"</td><td>"+data[i][0].valor+"</td></tr>");
		}


		 $('#dataTables-trazabilidad').dataTable( {
            "language":	
            {
            	 "url": "/assets/js/pluginspanishtabla"
            }
     	} );
	});
	
});

function mostrartrazabilidad(evento,sede,formulario,item,valor){
	$.ajax({	
			url:'/mostrartrazabilidad',
			type:'POST',
			data:{evento:evento,sede:sede,formulario:formulario,valor:valor,item:item},
			dataType:'html',
			success: function(resp){
				$("#page-wrapper").html(resp);	
			},
			error: function(jqXHR,estado,error){
				alert("error:"+error);
			},
			complete:function(jqXHR,estado){
			
			}
	});
}
$("#btncancelar").click(function(){	
	$(location).attr('href','/menuterminal');

});

