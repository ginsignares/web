@extends('layouts.listados')
@section('titulo')
	Listado de Clientes
@endsection
@section('tabla')
        			<table id="registro" class="table">
        				<thead>
	        				<tr>
	        					<th width="30%">
	        						Nit
	        					</th>
	        					<th width="40%">
	        						Razon social
	        					</th>
	        					<th align="center">
	        						Consultar
	        					</th>
	        					<th align="center">
	        						Borrar
	        					</th>
	        				</tr>
	        			</thead>
        				<tbody>
        					<?php
        						$filas="";
        						for ($i=0; $i < count($lista); $i++) { 
        							$filas.="<tr>";
        							$filas.="<td>".$lista[$i]->nit."</td><td>".$lista[$i]->razonsocial."</td><td><label onclick=consultar(".$lista[$i]->id.") class='btn btn-success btn-block'><i class='fa fa-check-circle'> Ver</label></td><td><label onclick=borrar(".$lista[$i]->id.") class='btn btn-danger btn-block'><i class='fa fa-times-circle'> Borrar</label></td>";
        							$filas.="</tr>";
        						}
        						echo $filas;
        					?>	
        				</tbody>

        		 </table>
        							
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $("#registro").DataTable({

    	 "language":	
            {
            	 "url": "/assets/js/pluginspanishtabla"
            }
    });
});
function consultar(id){

		$.ajax({	
			url:"/parametros/clientes",
			type:'get',
			data:{cliente_id:id},
			dataType:"json",
			success: function(resp){
				$("#frmclientes")[0].reset();	
				$("#cliente_id").val(id);
				$("#nit").val(resp.nit);
				$("#razonsocial").val(resp.razonsocial);
				$("#direccion").val(resp.direccion);
				$("#telefonos").val(resp.telefonos);
				$("#celular").val(resp.celular);
				$("#email").val(resp.email);
				$("#VentanaModal").fadeOut();
				$('.modal-backdrop').fadeOut();
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
} 

function borrar(id){
	swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de borrar el cliente seleccionado del sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Si",
					cancelButtonText:"No"

		},function (isConfirm){

			if (isConfirm){
				$.ajax({	
						url:"/borrarclientes",
						type:'post',
						data:{cliente_id:id},
						dataType:"json",
						success: function(resp){	
								$.notify("Los cambios se han sido realizados con exito","success");
							
						},
						error: function(jqXHR,estado,error){
							$.notify(error,"error");

						},
						complete:function(jqXHR,estado){
						
						}
				});

				$("#VentanaModal").fadeOut();
				$('.modal-backdrop').fadeOut();
			}

		});
}	
</script>
@endsection

