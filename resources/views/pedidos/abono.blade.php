<div class="modal-dialog">
<div class="container">
<div class="row">
<br>
    <div class="col-lg-8">

      <div class="panel panel-danger">
            <div class="panel-heading">
                Abono
            </div>
            <div class="panel-body">
                    <div class="form-group">
                            <label>Total a pagar</label>
                            <input type="text" id="saldo" name="saldo" class="form-control" disabled="true" value=<?php echo number_format($total)?>>
                    </div>           
            </div>    
            <div class="panel-body" align="right">
             <button type="button" class="btn btn-default" style='width: 100px' data-dismiss="modal">Cancelar</button>
             <button type="button" id="btnguardar" class="btn btn-success">Guardar</button>
            </div>
        </div>

</div>
</div>
</div>
</div>