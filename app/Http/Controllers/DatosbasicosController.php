<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Datosbasicosrequest;
use Validator;

class DatosbasicosController extends Controller
{

     public function __construct(){
            $this->middleware('auth');
          }
    public function guardar(Datosbasicosrequest $request){
    	$validator= Validator::make($request->all(),$request->rules(),$request->messages());
        if ($validator->valid()){
            if ($request->ajax()){
    	
		    	$datos=new \App\Datosbasicos;
		    	$datos->nit=$request['nit'];
		    	$datos->razonsocial=$request['razonsocial'];
		    	$datos->direccion=$request['direccion'];
		    	$datos->telefonos=$request['telefonos'];
		    	$datos->celular=$request['celular'];
		    	$datos->save();

		    	return response()->json(["valid"=>true]);
		    }
		}     	
    }

    public function actualizar(Datosbasicosrequest $request){
        $validator= Validator::make($request->all(),$request->rules(),$request->messages());
        if ($validator->valid()){
            if ($request->ajax()){
            	$datos=\App\Datosbasicos::find($request['datos_id']);
            	$datos->nit=$request['nit'];
            	$datos->razonsocial=$request['razonsocial'];
            	$datos->direccion=$request['direccion'];
            	$datos->telefonos=$request['telefonos'];
            	$datos->celular=$request['celular'];
            	$datos->save();

    	        return response()->json(["valid"=>true]);
            }
        }    

    }
}
