<div class="panel panel-danger">
<div class="panel-heading">
	<div class="row">
		<div class="col-lg-10">	
		 <h3 class="panel-title">Producto para facturar <br> <br> Cliente: {{$cliente->razonsocial}}</h3>
		
		 <input type='hidden' id='cliente_id' value={{$cliente->id}}>
		
		</div>
        <div class="col-lg-2">
                <div class="form-group">
                    <button type="button" class="btn btn-info btn-block" id="btndespachar" onclick='generardespacho()'>Facturar</button>
                </div>

        </div> 
    </div>
    <br>
    <div class="row">
        <div class="col-lg-2" >
             <div class="form-group">
                 <label>Forma de pago</label>
                            <select id="formapago" class="form-control">
                                <option value=0>Contado</option>
                                <option value=1>Credito</option>
                            </select>
             </div>
                   
        </div>
         <div class="col-lg-2">
             <div class="form-group">
                <label>Subtotal</label>
                <input type="text" id="subtotal" class="form-control" disabled="true">
            </div>
        </div>
        <div class="col-lg-2">
           <div class="form-group">
                <label>Descuento</label>
                <input type="text" id="descuento" class="form-control">
                <button type='button' onclick="descuento()" class="btn btn-primary">Cacular descuento</button>
            </div>    
         </div>   
        <div class="col-lg-2">       
             <div class="form-group">
                <label>Total</label>
                <input type="text" id="total" class="form-control" disabled="true">
                
            </div>
        </div>
		
	 </div>
</div>
<div class="panel-body">

       
       		<div class="table-responsive">
                                            <table id="tblclientes" class="table table-triped" >                
                                                         <tr>
                                                            <th>
                                                                Pedido
                                                            </th>
                                                            <th>
                                                                Producto
                                                            </th>
                                                            <th>
                                                                Cantidad
                                                            </th>
                                                            <th>
                                                                Pendiente
                                                            </th>
                                                            <th  width="10%"  >
                                                                Terminadas
                                                            </th>
                                                            <th  width="10%"  >
                                                                Facturar
                                                            </th>
                                                            <th width="10%" align="center">
                                                                Registrar
                                                            </th>
                                                             <th width="10%" align="center">
                                                                Registrado
                                                            </th>

                                                        </tr>
                                                        
                                                            <?php
                                                                
                                                                for ($i=0; $i < count($lista); $i++) { 
                                                                    $pendiente=$lista[$i]->cantidad-$lista[$i]->terminado;
                                                                    $terminado=$lista[$i]->terminado-($lista[$i]->entregado+$lista[$i]->rechazado);
                                                                    if ($terminado>0){
                                                                        echo "<tr>";
                                                                        echo "<td>".$lista[$i]->pedido_id."</td><td> ".$lista[$i]->nombre."</td><td>".$lista[$i]->cantidad."</td><td>".$pendiente."</td><td>".$terminado."</td><td><input type='text' class='form-control' id='Q".$lista[$i]->detalle_id."' ><input type='hidden' class='form-control' id='QV".$lista[$i]->detalle_id."' value=".$terminado."></td><td><label onclick=agregar(".$lista[$i]->detalle_id.") class='btn btn-success btn-block'><i class='fa fa-check-circle'> Registrar</label></td><td><input type='text' id='R".$lista[$i]->detalle_id."' class='form-control'  disabled=true></td>";
                                                                        echo "</tr>";
                                                                    }
                                                                }
                                                            ?>  
                                            </table>
                                        </div>
                                    </div>  
        
    </div>
</div>        

