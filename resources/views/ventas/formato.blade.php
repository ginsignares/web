	{!!Form::open(['id'=>'frmfactura'])!!} 
	<div class="panel panel-green">
        <div class="panel-heading">
            <h3 class="panel-title">Nueva Factura</h3>
        </div>
        <div class="panel-body">
        			<input type="hidden" class="form-control" id="pedido_id" name="pedido_id" value=0>
        		<div class="row">
        			<div class="col-lg-8">	
        				<div class="panel panel-info">
        				    <div class="panel-heading">
          					  <h3 class="panel-title">Información del cliente</h3>

       					    </div>
       					     <div class="panel-body">
			 					<div class="col-lg-6">		
					        			<div class="form-group">
					        					<input type="hidden" id="cliente_id" class="form-control" disabled="true" value={{$datos->id}}>
						            			<label>Nit</label>
											    <input type="text" class="form-control" disabled="true" value={{$datos->nit}}>
									    </div>
						            	<div class="form-group">
						            			<label>Razon social</label>
											    <input type="text" class="form-control" disabled="true" value={{$datos->razonsocial}}>
											   
									    </div>
									    <div class="form-group">
						            			<label for="nit">Direccion</label>
											    <input type="text" class="form-control" disabled="true"value={{$datos->direccion}}>
											   
									    </div>
								 </div>
								 <div class="col-lg-6">	
									    <div class="form-group">
						            			<label for="nit">Telefono</label>
											    <input type="text" class="form-control" disabled="true" value={{$datos->telefonos}}>
									    </div>
									     <div class="form-group">
						            			<label for="nit">Celular</label>
											    <input type="text" class="form-control" disabled="true" value={{$datos->celular}}>
									    </div>
									     <div class="form-group">
						            			<label for="nit">Email</label>
											    <input type="text" class="form-control" disabled="true" value={{$datos->email}}>
									    </div>
								</div>
							</div>	
						</div>
					</div>		
					<div class="col-lg-4">	
        				<div class="panel panel-yellow">
        				    <div class="panel-heading">
          					  <h3 class="panel-title">Información del factura</h3>
       					    </div>
       					     <div class="panel-body">
			 					<div class="col-lg-6">		
						            	<div class="form-group">
						            			<label>Fecha</label>
											    <input type="text" class="form-control" style="text-align:center" disabled="true" value={{$fecha}}>
											   
									    </div>
								</div>
								<div class="col-lg-6">
									    <div class="form-group">
						            			<label>Forma de pago</label>
											    <select id="formapago" class="form-control">
											    	<option value=0>Contado</option>
											    	<option value=1>Crédito</option>	

											    </select>
									    </div>
								 </div>
								 
							</div>	
						</div>
					</div>	
			   	</div>		 
			   	<div class="row">
        			<div class="col-lg-8">
        				<div class="panel panel-yellow">
        				    <div class="panel-heading">
          					  <h3 class="panel-title">Detalle del pedido</h3>
       					    </div>
       					    <div class="panel-body">
       					    	<div style="align:rigth">
       					    		<center>	
       					    			<button id="btnagregar" type="button" data-toggle="modal" class="btn btn-success"  data-target="#VentanaModal"><i style="font-size: 20px" class="fa fa-plus-circle fa-3x"></i></a> Agregar Productos1</button>
       					    		</center>	
       					    	</div>
       					    	<br>
       					    	<div class="table-responsive">
       					    		<table id="tbldetallepedido" class="table table-condensed"  >
       					    			<tr>
       					    				<th>#</th>
       					    				<th>Producto</th>
       					    				<th>Cantidad</th>
       					    				<th>Precio</th>
       					    				<th>Total</th>
       					    				<th align="center">Quitar</th>

       					    			<tr>
       					    			<tbody id="cpdetallepedido"></tbody>

       					    		</table>	

       					    	</div>
       					    </div>
       					</div>    

        			</div>
        			<div class="col-lg-4">
        				<div class="panel panel-yellow">
        				    <div class="panel-heading">
          					  <h3 class="panel-title">Totales del pedido</h3>
       					    </div>
       					     <div class="panel-body">
			 					<div class="col-lg-12">		
					        			<div class="form-group">
						            			<label>Subtotal</label>
											    <input type="text" id="subtotal" name="subtotal" class="form-control" disabled="true">
									    </div>
									   	<div class="form-group">
								    			<label>Descuento</label>
								    			<div class="form-group">
									    			<input type="text" id="descuento" name="descuento" 	class="form-control">
									    			<button type='button' id="btncalculardescuento" class="btn btn-primary">Calcular Descuento</button>
									    		</div>
												  
									    </div>
									    <div class="form-group">
						            			<label>Total</label>
											    <input type="text" id="total" name="total" class="form-control" disabled="true">
									    </div>
								 </div>
								 
							</div>	
						</div>
        			</div>
        		</div>   
	      
        </div>
        <div class="panel-footer">
        	<div class="form-group"  align="right">
        	  <button type="button" id="btncancelar" class="btn btn-default" style='width: 100px'>Cancelar</button>
        	 <button type="button" id="btnguardar" class="btn btn-success">Guardar</button>
        	</div> 
        </div>
    </div>
      </div>
   {!!Form::close()!!} 

{!!Html::script('assets/js/jquery.min.js')!!}
{!!Html::script('assets/js/ventas.js')!!}
<script src="/bootstrap/js/notify.min.js"></script>
 <script src="/sw/sweetalert.min.js"></script>

	

 