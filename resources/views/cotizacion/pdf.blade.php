<<!DOCTYPE html>
<html>
	<head>
		<title>Cotizacion Nº {{$factura}}</title>
			
		<style>
			.page-break {
			    page-break-after: always;
			}

		</style>
	</head>
<body>

<center>
<?php
	function encabezado($datosbasico,$datosfactura){
		$tabla="<div style='border: 2px solid #000;height:150px'>";
		$tabla.="<table>";
		$tabla.="<tr>";
		$tabla.="<td>";
		$tabla.="<div style='border: 1px solid #fff;width:480px;margin-left:10px;margin-top:0px;margin-bottom:1px;'>";
		$tabla.="<table>";
		$tabla.="<tr>";
		$tabla.="<th align='left'>".$datosbasico[0]->razonsocial."</th>";
		$tabla.="</tr>";
		$tabla.="<tr>";
		$tabla.="<td>".$datosbasico[0]->direccion."</td>";
		$tabla.="</tr>";
		$tabla.="<tr>";
		$tabla.="<td>Teléfonos: ".$datosbasico[0]->telefonos."</td>";
		$tabla.="</tr>";
		$tabla.="<tr>";
		$tabla.="<td>Celular: ".$datosbasico[0]->celular."</td>";
		$tabla.="</tr>";
		$tabla.="</table>";
		$tabla.="</div>";
		$tabla.="</td>";
		$tabla.="<td rowspan=2>";
		$tabla.="<div style='border: 1px solid #FFF;width:200px;margin-top:10px;margin-right:5px;height:250px'>";
		$tabla.="<table>
		<tr><td><B> COTIZACION <BR>N°: </B> COT-".$datosfactura->id."</td></tr>
		<tr><td><b> Fecha:</B> ".$datosfactura->created_at."</td></tr>
		
		</table>";
		$tabla.="</div>";
		$tabla.="</td>";

		$tabla.="</tr>";
		$tabla.="</table>";
		$tabla.="</div>";

	
		$tabla.="<br><div style='border: 2px solid #000;width:720px'>";
		$tabla.="<table>";
		$tabla.="<tr>";
		$tabla.="<td><b>Cliente:</b> ".$datosfactura->nombre."</td>";
		$tabla.="</tr>";
		$tabla.="<tr>";
		$tabla.="<td><b>Observaciones:</b> ".$datosfactura->observaciones."</td>";
		$tabla.="</tr>";
		$tabla.="</table>";
		$tabla.="</div>";
		
		return $tabla;
	}

	echo encabezado($datosbasico,$datosfactura);
	
	$tabla="<br><div>";
	$tabla.="<table style='width:720px'>";
	$tabla.="<thead style='border: 1px solid;'>";
	$tabla.="<tr>";
	$tabla.="<th>#</th>";
	$tabla.="<th>Producto</th>";
	$tabla.="<th>Cantidad</th>";
	$tabla.="<th>Precio</th>";
	$tabla.="<th>Total</th>";
	$tabla.="</tr>";
	$tabla.="</thead>";
	$tabla.="<tbody>";
	$saldo=0;
	$total=0;
	$subtotal=0;

	for ($i=0; $i < count($detalle); $i++) { 
		$numero=$i+1;
		$totalproducto=$detalle[$i]->cantidad*$detalle[$i]->precio;
		$subtotal+=$totalproducto;
		$tabla.="<tr>";
		$tabla.="<td>".$numero."</td>";
		$tabla.="<td>".$detalle[$i]->nombre."</td>";
		$tabla.="<td align='center'>".$detalle[$i]->cantidad."</td>";
		$tabla.="<td align='right'>$ ".number_format($detalle[$i]->precio)."</td>";
		$tabla.="<td align='right'>$ ".number_format($totalproducto)."</td>";
		$tabla.="</tr>";
		

	}
	$tabla.="</tbody>";
	$tabla.="</table>";
	$tabla.="</div>";
	echo $tabla;

	$total=$subtotal-$datosfactura->descuento;
	$saldo=$total-$datosfactura->abonado;

	$tabla="<br><div style='float:right'>";
	$tabla.="<table style='float:right'>";
	
	$tabla.="<tr style='border: 1px solid #fff'><th>Subtotal:</th><td align='right'>$ ".number_format($subtotal)."</td></tr>";
	$tabla.="<tr><th>Descuento:</th><td align='right'>$ ".number_format($datosfactura->descuento)."</td></tr>";
	$tabla.="<tr><th>Total:</th><td align='right'>$ ".number_format($total)."</td></tr>";	
	$tabla.="</table>";
	$tabla.="</div>";
	echo $tabla;
?>

</body>
</html>

