<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cotizacion extends Model
{
   use SoftDeletes;
   protected $table="cotizacion";
   protected $fillable=["nombre","observaciones","usuario_id","descuento","total"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
