@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
     <div id="viewformato" class="col-lg-12">           
        <div class="panel panel-green">
            <div class="panel-heading">
                <h3 class="panel-title">Saldos Iniciales</h3>
            </div>
            <div class="panel-body">
                
                                         <div class="table-responsive">
                                            <table id="tblclientes" class="table table-triped" >                
                                                   <thead>
                                                         <tr>
                                                            <th>
                                                                Producto
                                                            </th>
                                                            <th>
                                                                Cantidad
                                                            </th>
                                                            <th width="10%">
                                                                Guardar
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>    
                                                            <?php
                                                                for ($i=0; $i < count($lista); $i++) {

                                                                  echo "<tr><td>".$lista[$i]->nombre."</td><td><input type='text' id='Q".$lista[$i]->saldo_l_id."' style='width:60px' ></td><td><button type='button' class='btn btn-primary' onclick=guardarsaldo(".$lista[$i]->saldo_l_id.",".$lista[$i]->producto_id.")>Guardar</button></td></tr>";
                                                                }
                                                            ?>
                                                        </tbody>  
                                            </table>
                                        </div>
                                    </div>
                </div>        
        </div>
       {!!Form::close()!!} 
     </div>
    
</div>
</div>

@endsection 
@section('script')

<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({

         "language":    
            {
                 "url": "/assets/js/pluginspanishtabla"
            }
    });
}); 
    
function guardarsaldo(id,producto){

   var Q=$("#Q"+id).val();
     swal({
                    title:"!Advertencia¡",
                    text:"¿Esta seguro realizar cambios en el sistema?",
                    showCancelButton:true,
                    type:"warning",
                    confirmButtonColor:'#e92c43',
                    cancelButtonColor:'#e92c43',
                    confirmButtonText:"Aceptar"
                  
        },function (isConfirm){

            if (isConfirm){
                if (isNaN(Q)){
                   $("#Q"+id).focus(); 
                   $.notify("La cantida ingresada debe ser númerica","error");
                   return false;
                }    
                if (Q<0){
                   $("#Q"+id).focus(); 
                   $.notify("La cantida ingresada debe ser mayor a 0","error");
                   return false;
                }    

                 $.ajax({    
                            url:"/inventarios/saldosiniciales",
                            type:'post',
                            data:{saldo_l_id:id,cantidad:Q,producto:producto},
                            dataType:"json",
                            success: function(resp){    
                                    $.notify("Los cambios se han sido realizados con exito","success");
                                  $(location).attr('href','/menu/saldosiniciales');
                                
                            },
                            error: function(jqXHR,estado,error){
                                $.notify(error,"error");

                            },
                            complete:function(jqXHR,estado){
                            
                            }
                });
            }

        });

} 
  
</script>
@endsection

