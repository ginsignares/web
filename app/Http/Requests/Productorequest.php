<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class Productorequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'codigo'=>'required',
            'nombre'=>'required',
            'precio'=>'required|numeric'
        ];
    }

      public function messages()
    {
        return [
        'codigo.required'=>'El codigo es obligatorio',
        'nombre.required'=>'El nombre es obligatoria',
        'precio.required'=>'El precio es obligatoria',
       
       

        ];
    }

    public function response(array $errors){
        if ($this->ajax()){
            return response()->json($errors,200);
        }   
     
    }
}
