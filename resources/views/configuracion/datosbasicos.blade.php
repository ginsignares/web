@extends('layouts.menu')
@section('contenido')
<div class="row">
 <div class="col-lg-12">
 @if ($opcion==0)
	{!!Form::open(['id'=>'frmdatosbasicos'])!!} 
 @else  
 	{!!Form::model($datos,['id'=>'frmdatosbasicos'])!!}
 @endif	 

	<div class="panel panel-green">
        <div class="panel-heading">
            <h3 class="panel-title">Datos Básicos</h3>
        </div>
        <div class="panel-body">
        	
        		
	            	<div class="form-group">
	            			<label >Nit</label>
	            			@if ($opcion==0)
	            				<input type="hidden" class="form-control" id="datos_id" value=0>
						    @else
						    	<input type="hidden" class="form-control" id="datos_id" name="datos_id" value={{$datos->id}}>
						    @endif	
						    
						    {!!Form::text('nit',null,['id'=>'nit','class'=>'form-control'])!!}
						   
				    </div>
				    <div class="form-group">
	            			<label>Razon social</label>
	            			 {!!Form::text('razonsocial',null,['id'=>'razonsocial','class'=>'form-control'])!!}
						    
						   
				    </div>
				    <div class="form-group">
	            			<label >Direccion</label>
						    {!!Form::text('direccion',null,['id'=>'direccion','class'=>'form-control'])!!}
						   
				    </div>
				    <div class="form-group">
	            			<label >Telefono</label>
						  	{!!Form::text('telefonos',null,['id'=>'telefonos','class'=>'form-control'])!!}
						   
				    </div>
				    <div class="form-group">
	            			<label >Celular</label>
						    {!!Form::text('celular',null,['id'=>'celular','class'=>'form-control'])!!}
						   
				    </div>
	            </form>
        </div>
        <div class="panel-footer">
        	<div class="form-group"  align="right">
        	 <button type="submit" class="btn btn-success">Guardar</button>
        	</div> 
        </div>
    </div>
     {!!Form::close()!!} 	
 </div>
</div>
{!!Html::script('assets/js/jquery.min.js')!!}
{!!Html::script('assets/js/datosbasicos.js')!!}

@endsection	

