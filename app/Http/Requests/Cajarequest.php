<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class Cajarequest extends Request
{
     public function authorize()
    {
        return true;
    }
   public function rules()
    {
        return [
            'saldo'=>'required',
        ];
    }

     public function messages()
    {
        return [
        'saldo.required'=>'El saldo es obligatorio',
        ];
    }
    public function response(array $errors){
        if ($this->ajax()){
            return response()->json($errors,200);
        }   
     
    }
}
