<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

       <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="/www/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/www/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/www/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/www/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

   
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
        <style>
    body  {
        background-attachment: fixed;
        background-position: center top;
        background-size: 100%;
        background-color:#000000;
    }
    .modal-header, h2, .close {
      background-color: #00000;
      color:white !important;
      text-align: center;
      font-size: 25px;
    }
    </style>
</head>

<body>

 <div class="container">

        <div class="row">
            <div class="col-md-4 col-md-offset-4">
    
                <div class="login-panel panel panel-green">
                    <div class="panel-heading">
                        <center><h3><i class="fa fa-user"></i><span style="color:#ffab24"></span></h3></center>


                    </div>
                    <div class="panel-body">
                           <div id="menindex"></div> 
                            
                            <div class="panel-group" id="accordion">
                                <div class="panel panel-green">
                                   
                                        <div class="panel-body">
                                           {!!Form::open(['route'=>'log.store','method'=>'POST'])!!}
                                               
                                                <fieldset>
                                                    <br>
                                                    <div class="form-group">
                                                        <input class="form-control"  id="opcion" name="opcion" type="hidden" value=0 autofocus>
                                                        <input class="form-control" placeholder="Usuario" name="name" type="text" autofocus>
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="Contraseña" name="password" type="password" value="">
                                                    </div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input name="remember" type="checkbox" value="Remember Me">Recordar mi usuario
                                                        </label>
                                                    </div>
                                                    {!!Form::submit('Entrar',['id'=>'Login','class'=>'btn btn-lg btn-success btn-block'])!!}
                                                </fieldset>
                                            {!!Form::close()!!}
                                        </div>
                                  
                                </div>
                                
                           
                            </div>
                        </div>
                       


                      

                </div>
            </div>
        </div>
    </div>
   

 <!-- jQuery -->
    <script src="/www/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="/www/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="/www/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="/www/dist/js/sb-admin-2.js"></script>
   
    {!!Html::script('assets/js/script.js')!!}
   
</body>
</body>

</html>

