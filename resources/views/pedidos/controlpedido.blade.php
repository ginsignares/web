@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
	 <div id="viewformato" class="col-lg-12">			
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Pedidos</h3>
	        </div>
	        <div class="panel-body">
	        	
								         <div class="table-responsive">
								       		<table id="tblclientes" class="table table-triped" > 
								       			<thead>       		
									    				 <tr>
								        					<th>
								        						Factura
								        					</th>
								        					<th>
								        						Fecha de entrega
								        					</th>
								        					<th>
								        						Cliente
								        					</th>
								        					<th>
								        						Producto
								        					</th>
								        					<th>
								        						Cantidad
								        					</th>
								        					<th>
								        						Pendiente
								        					</th>
								        					<th  width="10%"  >
								        						Terminadas
								        					</th>
								        					<th width="10%" align="center">
								        						Guardar
								        					</th>
								        				</tr>
								        			</thead>
								        			<tbody>
								        					<?php
								        						
								        						for ($i=0; $i < count($lista); $i++) { 
								        							$pendiente=$lista[$i]->cantidad-$lista[$i]->terminado;
								        							if ($pendiente>0){
									        							echo "<tr>";
									        							echo "<td> FAC-".$lista[$i]->factura_id."</td><td>".$lista[$i]->fechaentrega."</td><td>".$lista[$i]->razonsocial."</td><td> ".$lista[$i]->nombre."</td><td>".$lista[$i]->cantidad."</td><td>".$pendiente."</td><td><input type='text' class='form-control' id='Q".$lista[$i]->detalle_id."' ><input type='hidden' class='form-control' id='Qp".$lista[$i]->detalle_id."' value=".$pendiente."></td><td><label onclick=grabaritem(".$lista[$i]->detalle_id.") class='btn btn-success btn-block'><i class='fa fa-check-circle'> Guardar</label></td>";
									        							echo "</tr>";
								        							}
								        						}
								        					?>	
								        				</tbody>
								          	</table>
								        </div>
								    </div>
				</div>        
	    </div>
	   {!!Form::close()!!} 
	 </div>
	
</div>
</div>

@endsection	
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({
         "language":    
            {
                 "url": "/assets/js/pluginspanishtabla"
            }
    });
});

</script>
{!!Html::script('assets/js/formato.js')!!}

@endsection	