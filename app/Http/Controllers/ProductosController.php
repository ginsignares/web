<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Productorequest;
use Validator;
use DB;

class ProductosController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function existencias(Request $requests){
            $tabla=  DB::table('productos')->select('productos.id as producto_id','productos.nombre','saldos.id as saldo_id','saldos_localizaciones.id as saldo_l_id','saldos_localizaciones.saldos')->join('saldos','saldos.producto_id','=','productos.id')->join('saldos_localizaciones','saldos_localizaciones.saldo_id','=','saldos.id')->where('saldos_localizaciones.saldos','>',0)->where('saldos_localizaciones.localizacion_id',$requests->localizacion)->get();

             return view('informes.existencias',['lista'=>$tabla,'localizacion'=>$requests->localizacion]);
         
    }


   


    public function saldosiniciales(Request $request){
             $saldos_localizacion= \App\SaldosLocalizacion::find($request->saldo_l_id);
             $saldos_localizacion->inv_inicial=$request->cantidad;
             $saldos_localizacion->entradas=0;
             $saldos_localizacion->salidas=0;
             $saldos_localizacion->saldos=$request->cantidad;
             $saldos_localizacion->save();

             $kardex=new \App\Kardex;
             $kardex->concepto="Inv. Inicial";
             $kardex->operacion=1;
             $kardex->localizacion=1;
             $kardex->save();

             $detallekardex=new \App\DetalleKardex;
             $detallekardex->kardex_id=$kardex->id;
             $detallekardex->producto_id=$request->producto;
             $detallekardex->cantidad=$request->cantidad;
             $detallekardex->precio=0;
             $detallekardex->save();

             return response()->json(["valid"=>true]);
    }

    public function listado(){
   		$datos=\App\Productos::where('deleted_at',null)->get();
   		return view('configuracion.listadoproductos',['lista'=>$datos]);
   }

   public function guardar(Productorequest $request){
    	$validator= Validator::make($request->all(),$request->rules(),$request->messages());
        if ($validator->valid()){
            if ($request->ajax()){
                $datos=\App\Productos::where('codigo',$request['codigo'])->where('deleted_at',null)->first();
                if ($datos!=null)
                    return response()->json(["codigo"=>'El codigo ya existe']);

                $datos=\App\Productos::where('nombre',$request['nombre'])->where('deleted_at',null)->first();
                if ($datos!=null)
                    return response()->json(["nombre"=>'El nombre ya existe']);
                
		    	$datos=new \App\Productos;
		    	$datos->codigo=$request['codigo'];
		    	$datos->nombre=$request['nombre'];
		    	$datos->precio=$request['precio'];
                $datos->fabricado=$request['fabricado'];
		    	$datos->ubicacion=$request['ubicacion'];
		    	$datos->descripcion=$request['descripcion'];
		    	$datos->save();

                $saldos= new \App\Saldos;
                $saldos->producto_id=$datos->id;
                $saldos->save();

                $saldos_localizacion= new \App\SaldosLocalizacion;
                $saldos_localizacion->saldo_id=$saldos->id;
                $saldos_localizacion->localizacion_id=1;
                $saldos_localizacion->inv_inicial=0;
                $saldos_localizacion->save();

                if ($request['fabricado']=="Si"){
                    $saldos_localizacion= new \App\SaldosLocalizacion;
                    $saldos_localizacion->saldo_id=$saldos->id;
                    $saldos_localizacion->localizacion_id=3;
                    $saldos_localizacion->inv_inicial=0;
                    $saldos_localizacion->save();
                }
		    	return response()->json(["valid"=>true]);
		    }
		}   	
    }

    public function update(Productorequest $request){

        $validator= Validator::make($request->all(),$request->rules(),$request->messages());
        if ($validator->valid()){
            if ($request->ajax()){
                $datos=\App\Productos::where('codigo',$request['codigo'])->where('deleted_at',null)->first();
                if ($datos!=null)
                {
                    if ($datos->id!=$request['producto_id'])
                        return response()->json(["codigo"=>'El codigo ya existe']);
                }

                $datos=\App\Productos::where('nombre',$request['nombre'])->where('deleted_at',null)->first();
                if ($datos!=null){
                    if ($datos->id!=$request['producto_id'])
                        return response()->json(["nombre"=>'El nombre ya existe']);
                }
            
                $datos=\App\Productos::find($request['producto_id']);
		    	$datos->codigo=$request['codigo'];
		    	$datos->nombre=$request['nombre'];
		    	$datos->precio=$request['precio'];
                $datos->fabricado=$request['fabricado'];
		    	$datos->ubicacion=$request['ubicacion'];
		    	$datos->descripcion=$request['descripcion'];
		    	$datos->save();
		    	return response()->json(["valid"=>true]);
            }
        }    

    }

    public function borrar(Request $request){
        $datos=\App\Productos::find($request['producto_id']);
        $datos->deleted_at= date('Y-m-d H:s:i');
        $datos->save();
        return response()->json(["valid"=>true]);
    }
}


