<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleCotizacion extends Model
{
    protected $table="detallecotizacion";
    protected $fillable=["cotizacion_id","producto_id","cantidad","precio"]; 
    public $timestamps = false;
}
