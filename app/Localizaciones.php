<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Localizaciones extends Model
{
   protected $table="localizaciones";
   protected $fillable=["nombre"];
}
