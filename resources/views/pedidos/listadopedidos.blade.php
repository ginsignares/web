@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
	 <div id="viewformato" class="col-lg-12">			
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Pedidos terminados</h3>
	        </div>
	        <div class="panel-body">
	        	
								         <div class="table-responsive">
								       		<table id="tblclientes" class="table table-triped" >      <thead>  		
									    				 <tr>
								        					<th>
								        						Numero
								        					</th>
								        					<th>
								        						Cliente
								        					</th>
								        					<th>
								        						Fecha de <br>entrega
								        					</th>
								        					<th>
								        						Fecha de <br>creación
								        					</th>
								        					<th>
								        						Estado
								        					</th>
								        					<th width="10%" align="center">
								        						Seleccionar
								        					</th>
								        					<th width="10%" align="center">
								        						Resumen
								        					</th>
								        					<th width="10%" align="center">
								        						Terminar 
								        					</th>
								        					<th width="10%" align="center">
								        						Entregar 
								        					</th>

								        				</tr>
								        			</thead>
								        			<tbody>	
								        					<?php
								        						
								        						for ($i=0; $i < count($lista); $i++) { 
								        							echo "<tr>";
								        							echo "<td> FAC-".$lista[$i]->factura_id."</td><td>".$lista[$i]->razonsocial."</td><td> ".$lista[$i]->fechaentrega."</td><td>".$lista[$i]->created_at."</td><td>".$lista[$i]->estado."</td>

								        							<td><label onclick=ver(".$lista[$i]->factura_id.") class='btn btn-success btn-block'><i class='fa fa-check-circle'> Ver</label></td>

								        							<td><label onclick=resumen(".$lista[$i]->factura_id.") class='btn btn-info btn-block'><i class='fa fa-check-circle'> Resumen</label></td>";

								        							if ($lista[$i]->estado=="Terminado" || $lista[$i]->estado=="Entregado" )


								        							echo "<td><label   class='btn btn-warning btn-block'><i class='fa fa-times-circle'> Terminar</label></td>";
								        							else
								        								echo "<td><label onclick=terminar(".$lista[$i]->factura_id.")  class='btn btn-danger btn-block'><i class='fa fa-times-circle'> Terminar</label></td>";

								        							if ($lista[$i]->estado=="Entregado")


								        							echo "<td><label   class='btn-block'><i class='fa fa-times-circle'> Entregar</label></td>";
								        							else
								        								echo "<td><label onclick=entregar(".$lista[$i]->factura_id.")  class='btn btn-primary btn-block'><i class='fa fa-times-circle'> Entregar</label></td>";

								        							echo "</tr>";
								        						}
								        					?>
								        			</tbody>	
								          	</table>
								        </div>
								    </div>
				</div>        
	    </div>
	   {!!Form::close()!!} 
	 </div>
	
</div>
</div>

@endsection	
@section('script')
<script type="text/javascript">
//onclick=terminar(".$lista[$i]->factura_id.")
$(document).ready(function(){
    $("#tblclientes").DataTable({

    	 "language":	
            {
            	 "url": "/assets/js/pluginspanishtabla"
            }
    });
});	

function resumen(id){
	 window.open('/generarPDFresumen/'+id, '_blank');
}


function terminar(id){


	
		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de dar por terminado el pedido de la factura N°"+id+"?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
			
			if(isConfirm)
			{
				$.ajax({	
						url:"/actulizarproductoglobal",
						type:'post',
						data:{factura:id},
						dataType:"html",
						success: function(resp){
								    
										//$.notify(resp.texto);
										$.notify("Los cambios se han sido realizados con exito","success");
										
										$(location).attr('href','/listadopedido');
									
						},
						error: function(jqXHR,estado,error){
							$.notify(error,"error");

						},
						complete:function(jqXHR,estado){
						
						}
				});

			}	


		});
}

function entregar(id){


	
		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de dar por entregado el pedido  la factura N°"+id+"?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
			
			if(isConfirm)
			{
				$.ajax({	
						url:"/actulizarproductoglobal2",
						type:'post',
						data:{factura:id},
						dataType:"html",
						success: function(resp){
								    
										//$.notify(resp.texto);
										$.notify("Los cambios se han sido realizados con exito","success");
										
										$(location).attr('href','/listadopedido');
									
						},
						error: function(jqXHR,estado,error){
							$.notify(error,"error");

						},
						complete:function(jqXHR,estado){
						
						}
				});

			}	


		});
}
</script>
{!!Html::script('assets/js/formato.js')!!}
@endsection

