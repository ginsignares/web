<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class FacturaController extends Controller
{
    public function formato(Request $requests){
        if ($requests['formato_id']==0){
          $tabla = DB::table('clientes')->select()->where('id',$requests->cliente_id)->where('deleted_at',null)->first(); 
             return view('ventas.formato',['datos'=>$tabla,'fecha'=>date("Y-m-d")]);
        }
       
    }   
    public function formatoproductos(Request $requests){
             $tabla = DB::table('productos')->select('productos.id as producto_id','productos.nombre','productos.precio','saldos_localizaciones.saldos')->join('saldos','saldos.producto_id','=','productos.id')->join('saldos_localizaciones','saldos_localizaciones.saldo_id','=','saldos.id')->where('saldos_localizaciones.localizacion_id',1)->where('productos.deleted_at',null)->get(); 
           
             return view('ventas.listadoproductos',['lista'=>$tabla]);
    }    

      public function guardar(Request $requests){
            $cierre= DB::table('cierres')->select()->OrderBy('id','desc')->first();
           
            if ($cierre==null)
                return response()->json(['mensaje'=>true]);

             $factura= new \App\Factura;
             $factura->cliente_id=$requests->cliente_id;
             $factura->formapago=$requests->formapago;
             if ($requests->formapago==0)
                $factura->estado="Pagada";
             else
                $factura->estado="Pendiente";
             $factura->descuento=$requests->descuento;
             $factura->total=$requests->valor;
             $factura->usuario_id=\Auth::user()->id;
             $factura->save();
             $nmofactura=$factura->id;

            if ($requests->formapago==0){
                $documento=new \App\Documentos;
                $documento->concepto_id=1;
                $documento->cierre_id=$cierre->id;
                $documento->detalle="FAC # ".$nmofactura;
                $documento->valor=$requests->valor;
                $documento->usuario_id=\Auth::user()->id;
                $documento->save();
            }    
             $kardex= new \App\Kardex;
             $kardex->concepto="VENTA FAC #".$nmofactura;
             $kardex->operacion=2;
             $kardex->localizacion=1;
             $kardex->save();

             for ($i=0; $i < count($requests->productos) ; $i++) {
                if ($requests->productos[$i]['estado']==0){ 
                  $detalle=new \App\DetalleFactura;  
                  $detalle->factura_id=$nmofactura;
                  $detalle->producto_id=$requests->productos[$i]['producto_id'];
                  $detalle->cantidad=$requests->productos[$i]['cantidad'];
                  $detalle->terminado=$requests->productos[$i]['cantidad'];
                  $detalle->entregado=$requests->productos[$i]['cantidad'];
                  $detalle->precio=$requests->productos[$i]['precio'];
                  $detalle->save();

                  $detallekardex=new \App\DetalleKardex;
                  $detallekardex->kardex_id=$kardex->id;
                  $detallekardex->producto_id=$requests->productos[$i]['producto_id'];
                  $detallekardex->cantidad=-$requests->productos[$i]['cantidad'];
                  $detallekardex->precio=$requests->productos[$i]['precio'];
                  $detallekardex->save();

                  $saldo=\App\Saldos::where('producto_id',$requests->productos[$i]['producto_id'])->first();
                  $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->where('localizacion_id',1)->first();
                  $saldo_localizacion->salidas=$saldo_localizacion->salidas+$requests->productos[$i]['cantidad'];
                  $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
                  $saldo_localizacion->saldos=$nuevosaldo;
                  $saldo_localizacion->save();

                }   
             }

             return response()->json(['valid'=>true,'factura'=>$nmofactura]);
      }  
}
