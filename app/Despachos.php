<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\SoftDeletes;

class Despachos extends Model
{
   use SoftDeletes;
   protected $table="depachos";
   protected $fillable=["cliente_id","usuario_id"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
