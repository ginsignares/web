<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Datosbasicos extends Model
{
    use SoftDeletes;
    protected $table="datosbasicos";
    protected $fillable=["nit","razonsocial","direccion","telefonos","celular"];
    protected $dates=["deleted_at"];
    protected $hidden=["deleted_at"];

}
