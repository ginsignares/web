@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
 <div class="col-lg-12">
  @if ($opcion==0)
	{!!Form::open(['id'=>'frmproveedores'])!!} 
 @else  
 	{!!Form::model($datos,['id'=>'frmproveedores'])!!}
 @endif	 

	<div class="panel panel-green">
        <div class="panel-heading">
            <h3 class="panel-title">Proveedores</h3>
        </div>
        <div class="panel-body">
        	
	            	<div class="form-group">
	            			<label>Nit</label>
	            				<input type="hidden" class="form-control" id="proveedor_id" name="proveedor_id"  value=0> 
						    {!!Form::text('nit',null,['id'=>'nit','class'=>'form-control'])!!}
				    </div>
				    <div class="form-group">
	            			<label>Razon social</label>
						    {!!Form::text('razonsocial',null,['id'=>'razonsocial','class'=>'form-control'])!!}
						   
				    </div>
				    <div class="form-group">
	            			<label>Direccion</label>
						     {!!Form::text('direccion',null,['id'=>'direccion','class'=>'form-control'])!!}
						   
				    </div>
				    <div class="form-group">
	            			<label>Telefono</label>
						    {!!Form::text('telefonos',null,['id'=>'telefonos','class'=>'form-control'])!!}
						   
				    </div>
				    <div class="form-group">
	            			<label>Celular</label>
	            			 {!!Form::text('celular',null,['id'=>'celular','class'=>'form-control'])!!}
				    </div>
				      <div class="form-group">
	            			<label>Email</label>
	            			 {!!Form::text('email',null,['id'=>'email','class'=>'form-control'])!!}
				    </div>
	        
        </div>
        <div class="panel-footer">
        	<div class="form-group"  align="right">
        	 <button type="button" id="consultar"   data-toggle="modal" data-target="#VentanaModal" class="btn btn-default">Consultar</button>
        	 <button type="submit" class="btn btn-success">Guardar</button>
        	</div> 
        </div>
    </div>
       {!!Form::close()!!} 
 </div>
</div>
@endsection	
@section('script')
	{!!Html::script('assets/js/proveedores.js')!!}
@endsection