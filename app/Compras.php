<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Compras extends Model
{
   use SoftDeletes;
   protected $table="compras";
   protected $fillable=["proveedor_id","factura","fechacompra","observaciones","usuario_id"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
