@extends('layouts.menu')
@section('contenido')

	<div class="row">
	 <div class="col-lg-2">
	 	{!!Form::open(['id'=>'frmsaldos'])!!} 
	 
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Caja</h3>
	        </div>
	        <div class="panel-body">
	        	
		            	<div class="form-group">
		            			<label>Saldo inicial:</label>
							    {!!Form::text('saldo',null,['id'=>'saldo','class'=>'form-control'])!!}
					    </div>
					    
	        </div>
	        <div class="panel-footer">
	        	<div class="form-group"  align="right">
	        	 <button type="submit" class="btn btn-success">Aceptar</button>
	        	</div> 
	        </div>
	    </div>
	       {!!Form::close()!!} 
	 </div>
	</div>

{!!Html::script('assets/js/jquery.min.js')!!}
{!!Html::script('assets/js/caja.js')!!}
@endsection	

