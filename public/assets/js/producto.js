$(function(){
   $("#frmproducto").on("submit", function(e){
   		var datos= $("#producto_id").val();
   		var url="";
   		var mensaje="";
   	 	var fields= $(this).serialize();
   		if (datos==0)
   			url="/grabarproducto";
   		else
   			url="/actualizarproducto";
   				
   			

  		console.log(url+" "+fields);
   		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){

			if (isConfirm){
					$.ajax({	
							url:url,
							type:'POST',
							data:fields,
							 dataType: "json",
							success: function(resp){	
								if (resp.valid !==undefined){
									$.notify("Los cambios se han sido realizados con exito","success");
									$("#frmproducto")[0].reset();
								}
								else
								{	
									if(resp.codigo!==undefined){
										$("#codigo").focus();
										$.notify(resp.codigo,"error");
										return false;
									}
									if(resp.nombre!==undefined){
										$("#nombre").focus();
										$.notify(resp.nombre,"error");
										return false;
									}
									if(resp.precio!==undefined){
										$("#precio").focus();
										$.notify(resp.precio,"error");
										return false;
									}
									if(resp.ubicacion!==undefined){
										$("#ubicacion").focus();
										$.notify(resp.ubicacion,"error");
										return false;
									}
									if(resp.descripcion!==undefined){
										$("#descripcion").focus();
										$.notify(resp.descripcion,"error");
										return false;
									}
									

								}		
							},
							error: function(jqXHR,estado,error){
								$.notify(error,"error");

							},
							complete:function(jqXHR,estado){
							
							}
					});



			}

		});


		
   		return false;        
    });
}); 
$("#consultar").click(function(){

	$.ajax({	
							url:"/listadoproducto",
							type:'GET',
							dataType:"html",
							success: function(resp){	
								$("#VentanaModal").html('');
								$("#VentanaModal").html(resp);
							},
							error: function(jqXHR,estado,error){
								$.notify(error,"error");

							},
							complete:function(jqXHR,estado){
							
							}
					});


});	