@extends('layouts.menu')
@section('contenido')

      <div class="panel panel-green">
            <div class="panel-heading">
                Historico de caja
            </div>
            <div class="panel-body">
                       <div class="row" style="align:right">
                          <div class="col-lg-5">
                            <label><spam>Consultar facturas desde: 
                            <input type="date" id="txtfechai" name="txtfechaf" value=<?php echo date('Y-m-d')?>> a 
                            <input type="date" id="txtfechaf" name="txtfechaf" value=<?php echo date('Y-m-d')?>>
                            </spam>
                            </label>
                             <button id="btnconsultar" class="btn btn-info btn-block">Consultar</button>   
                            
                          </div>  
                       </div>
                       <br>
                       <div class="row">
                             <div class="col-lg-12">
                                <table id="registro" class="table">
                                    <thead>
                                         <tr>
                                            <th>
                                                Fecha
                                            </th>
                                             <th>
                                                Concepto
                                            </th>
                                            <th>
                                                Detalle
                                            </th>
                                            <th>
                                                Valor
                                            </th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody id="fila">
                                        
                                                           
                                        
                                         
                                    </tbody>
                            </table>        
                            <table style="align:right">
                                            <?php
                                                        echo "<tr>";
                                                        echo "<td colspan=4><h1>Total: </h1> </td>";
                                                        echo "<td   style='color:#FF0000'><h1 id='saldo'></h1></td>";

                                                        echo "</tr>";
                                            ?>
                                    </table>    
                            </div>
                    </div>        
            </div>    
        </div>

@endsection
@section('script')
<script type="text/javascript">
var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft  +splitRight;
 },
 new:function(num, simbol){
  this.simbol = simbol ||'';
  return this.formatear(num);
 }
}
function consultar(id){
    $(location).attr('href',"/controlpedido/"+id);
}
$("#btnconsultar").click(function(){

    var fi=$("#txtfechai").val();
    var ff=$("#txtfechaf").val();

    $.ajax({    
                            url:"/listadocajafecha",
                            type:'POST',
                            data:{fi:fi,ff:ff},
                            dataType:"json",
                            success: function(resp){    
                                var tabla=$("#fila");        
                                var acumulatotal=0;
                                var acumuladescuento=0;
                                var acumulasaldo=0;
                                var acumulabonado=0;
                                tabla.html('');
                                for (var i =0; i < resp.length; i++) {
                                  
                                 	acumulatotal+=resp[i].valor;

                                     tabla.append("<tr><td>"+resp[i].created_at+"</td><td>"+resp[i].nombre+"</td><td>"+resp[i].detalle+"</td><td>"+resp[i].valor+"</td></tr>");  

                                }
                                  $("#saldo").html(formatNumber.new(acumulatotal));
                                
                            },
                            error: function(jqXHR,estado,error){
                                alert(error);

                            },
                            complete:function(jqXHR,estado){
                               
                            }
                    });


});
function ver(id){
     window.open('/generarPDF/'+id, '_blank');
}

</script>
@endsection 
