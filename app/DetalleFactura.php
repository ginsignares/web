<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleFactura extends Model
{
   protected $table="detallefactura";
   protected $fillable=["factura_id","producto_id","cantidad","precio","entregado"];
    public $timestamps = false;

}
