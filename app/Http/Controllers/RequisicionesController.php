<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;

class RequisicionesController extends Controller
{
    public function formato(){

        return view('requisiciones.formato');
    }
     public function formatoproductos(Request $requests){
             $tabla = DB::table('productos')->select('productos.id as producto_id','productos.nombre','productos.precio','saldos_localizaciones.saldos')->join('saldos','saldos.producto_id','=','productos.id')->join('saldos_localizaciones','saldos_localizaciones.saldo_id','=','saldos.id')->where('productos.deleted_at',null)->where('saldos_localizaciones.localizacion_id',1)->where('productos.fabricado','No')->get(); 
             return view('requisiciones.listadoproductos',['lista'=>$tabla]);
    }    
     public function guardar(Request $requests){
            
             $requisiciones= new \App\Requisiciones;
           	 $requisiciones->nombre=$requests->nombre;
             $requisiciones->observaciones=$requests->observaciones;
             $requisiciones->usuario_id=\Auth::user()->id;
             $requisiciones->save();


          
             $kardex= new \App\Kardex;
             $kardex->concepto="REQ #".$requisiciones->id;
             $kardex->operacion=2;
             $kardex->localizacion=1;
             $kardex->save();

             for ($i=0; $i < count($requests->productos) ; $i++) {
                if ($requests->productos[$i]['estado']==0){ 
                  $detalle=new \App\DetalleRequisiciones;  
                  $detalle->requisicion_id=$requisiciones->id;
                  $detalle->producto_id=$requests->productos[$i]['producto_id'];
                  $detalle->cantidad=$requests->productos[$i]['cantidad'];
                  $detalle->save();

                  $detallekardex=new \App\DetalleKardex;
                  $detallekardex->kardex_id=$kardex->id;
                  $detallekardex->producto_id=$requests->productos[$i]['producto_id'];
                  $detallekardex->cantidad=-$requests->productos[$i]['cantidad'];
                  $detallekardex->precio=$requests->productos[$i]['precio'];
                  $detallekardex->save();

                  $saldo=\App\Saldos::where('producto_id',$requests->productos[$i]['producto_id'])->first();
                  $saldo_localizacion=\App\SaldosLocalizacion::where('saldo_id',$saldo->id)->first();
                  $saldo_localizacion->salidas=$saldo_localizacion->salidas+$requests->productos[$i]['cantidad'];
                  $nuevosaldo=($saldo_localizacion->inv_inicial+$saldo_localizacion->entradas)-$saldo_localizacion->salidas;
                  $saldo_localizacion->saldos=$nuevosaldo;
                  $saldo_localizacion->save();

                }   
             }

             return response()->json(['valid'=>true]);
      }  
}
