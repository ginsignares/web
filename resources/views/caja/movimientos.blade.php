<div class="modal-dialog">
<div class="container">
<div class="row">
<br>
    <div class="col-lg-8">
      <div class="panel panel-green">
            <div class="panel-heading">
              {{$titulo}}
            </div>
            <div class="panel-body">
              <div class="form-group">
                      <label>Concepto:</label>
                      <select id="concepto" class="form-control">
                            <?php
                                for ($i=0; $i < count($concepto) ; $i++) { 
                                  echo "<option value='".$concepto[$i]->id."'>".$concepto[$i]->nombre."</option>";
                                }
                            ?>

                      </select>
              </div>
        			<div class="form-group">
                      <label>Valor:</label>
                      {!!Form::text('valor',null,['id'=>'valor','class'=>'form-control'])!!}
              </div>

              <div class="form-group">
                      <label>Detalle:</label>
                      {!!Form::text('detalle',null,['id'=>'detalle','class'=>'form-control'])!!}
              </div>
            
        		
        		</table>
          
            </div>    
            <div class="panel-body" align="right">
              <button type="button"  class="btn btn-default" data-dismiss="modal">Cancelar</button>
               <button type="button" id="btnaceptar" class="btn btn-success">Aceptar</button>
              
            </div>
        </div>
     </div>    
</div>
</div>
</div>
{!!Html::script('assets/js/jquery.min.js')!!}
<script src="/bootstrap/js/notify.min.js"></script>
<script type="text/javascript">
 $("#btnaceptar").click(function(){
  var cierre=$("#cierre").val();
  var concepto=$("#concepto").val();
  var valor=$("#valor").val();
  var detalle=$("#detalle").val();
  var total=$("#total").val();

  if (isNaN(valor)==true  | (valor<=0)){
    $.notify("El valor ingresado no es valido","error");
    return;
  }
  swal({
          title:"!Advertencia¡",
          text:"¿Esta seguro de realizar los cambios en el sistema?",
          showCancelButton:true,
          type:"warning",
          confirmButtonColor:'#e92c43',
          cancelButtonColor:'#e92c43',
          confirmButtonText:"Aceptar"
    }, function (isConfirm){

      $.ajax({  
          url:'/guardarmovimiento',
          type:'post',
          data:{cierre:cierre,concepto:concepto,valor:valor,detalle:detalle,total:total},
          dataType: "json",
          success: function(resp){  
            if (resp.valid !==undefined){
              $.notify("Los cambios se han sido realizados con exito","success");
              $(location).attr('href','/menu/caja');
            }
            else
            {
              $.notify("Fondo insuficientes para realizar esta operación","error");

            }  
            
          },
          error: function(jqXHR,estado,error){
            $.notify(error,"error");

          },
          complete:function(jqXHR,estado){
          
          }
        });
    }); 
}); 

</script>>
