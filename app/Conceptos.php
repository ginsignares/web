<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Conceptos extends Model
{
   use SoftDeletes;
   protected $table="conceptos";
   protected $fillable=["nombre","tipo","sistema"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
