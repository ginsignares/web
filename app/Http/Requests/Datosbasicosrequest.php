<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class Datosbasicosrequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nit'=>'required',
            'razonsocial'=>'required',
            'direccion'=>'required',
            'telefonos'=>'required',
            'celular'=>'required'
        ];
    }

     public function messages()
    {
        return [
        'nit.required'=>'El nit es obligatorio',
        'razonsocial.required'=>'La razon social es obligatoria',
        'direccion.required'=>'La dirección es obligatoria',
        'telefonos.required'=>'El teléfono es obligatorio',
        'celular.required'=>'El celular es obligatorio'
        ];
    }

    public function response(array $errors){
        if ($this->ajax()){
            return response()->json($errors,200);
        }   
     
    }
}
