<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleRequisiciones extends Model
{
    protected $table="detallerequisicion";
    protected $fillable=["requisicion_id","producto_id","cantidad","precio"]; 
    public $timestamps = false;
}
