<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cierres extends Model
{
   use SoftDeletes;
   protected $table="cierres";
   protected $fillable=["fechainicio","fechacierre","usuario_id","usuariocierre_id"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
