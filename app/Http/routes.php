<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*Route::get('/usuario', function () {
   $usuario= new \App\User;
   $usuario->name="vendedor1";
   $usuario->password=bcrypt("vendedor1");
   $usuario->nivel=2;
    $usuario->email="--";
   $usuario->save();

});*/

Route::get('/', function () {
    return view('principal');
});

Route::get('/menu', function () {
  
  switch (Auth::user()->nivel) {
    case 0:
        return view('index');
      break;
    case 1:
        return view('index2');
        break;
   /* case 2:
        return view('index3');
        break;*/
    default:
      # code...
      break;
  }
 
});
Route::get('/pdf', function () {
	$pdf=PDF::loadView('pedidos.pdf');
  return $pdf->stream('factura.pdf');
});

Route::get('/saldos','FrontController@saldos');
Route::get('/parametros/datosbasicos','FrontController@datosbasico');
Route::get('/parametros/clientes','FrontController@clientes');
Route::get('/parametros/productos','FrontController@productos');
Route::get('/parametros/proveedores','FrontController@proveedores');
Route::get('/menu/cotizacion','FrontController@cotizacion');
Route::get('/menu/facturas','FrontController@pedidos');

Route::get('/menu/saldosiniciales','FrontController@saldosiniciales');
Route::get('/menu/compras','FrontController@compras');


Route::post('/grabardatosbasicos','DatosbasicosController@guardar');
Route::post('/actualizardatosbasicos','DatosbasicosController@actualizar');

Route::post('/grabarclientes','ClientesController@guardar');
Route::post('/actualizarclientes','ClientesController@update');
Route::get('/listadoclientes','ClientesController@listado');
Route::post('/borrarclientes','ClientesController@borrar');

Route::post('/grabarproducto','ProductosController@guardar');
Route::post('/actualizarproducto','ProductosController@update');
Route::get('/listadoproducto','ProductosController@listado');
Route::post('/borrarproducto','ProductosController@borrar');
Route::post('/inventarios/saldosiniciales','ProductosController@saldosiniciales');

Route::post('/grabarproveedor','ProveedoresController@guardar');
Route::post('/actualizarproveedor','ProveedoresController@update');
Route::get('/listadoproveedor','ProveedoresController@listado');
Route::post('/borrarproveedor','ProveedoresController@borrar');

Route::get('/formato','PedidosController@formato');
Route::get('/formatoproductos','PedidosController@formatoproductos');
Route::post('/obtenerproducto','PedidosController@obtenerproducto');
Route::post('/guardarpedido','PedidosController@guardar');
Route::get('/listadopedido','PedidosController@listadopedidos');
Route::get('/controlpedido/{producto_id}','PedidosController@controlpedidos');

Route::post('/controlpedidofechaentrega','PedidosController@controlpedidosporfecha');
Route::get('/listadofacturas','PedidosController@listadofacturas');
Route::get('/listadofacturasporfechapedido','PedidosController@listadofacturasporfecha');
Route::get('/facturarpedido','PedidosController@facturar');
Route::get('/obtenerproductosfacturar','PedidosController@obtenerproductosfacturar');
Route::post('/grabarfacturapedido','PedidosController@guardarfactura');
Route::post('/obtenerdetallepedido','PedidosController@obtenerdetallepedido');
Route::get('/abono','PedidosController@abono');
Route::get('/generarPDF/{factura}','PedidosController@generarPDF');
Route::get('/generarPDFresumen/{factura}','PedidosController@generarPDFresumen');


Route::post('/actulizarproducto','PedidosController@actulizarproducto');
Route::post('/actulizarproductoglobal','PedidosController@actulizarproductoglobal');
Route::post('/actulizarproductoglobal2','PedidosController@actulizarproductoglobal2');

Route::get('/menu/ventas','FrontController@ventas');
Route::get('/menu/formatoventas','FacturaController@formato');
Route::get('/menu/formatoproductos','FacturaController@formatoproductos');
Route::post('/menu/guardarventa','FacturaController@guardar');


Route::get('/menu/caja','CajaController@caja');
Route::post('/grabarsaldocaja','CajaController@saldo');
Route::post('/cierrecaja','CajaController@cierre');
Route::get('/movimientos','CajaController@movimientos');
Route::post('/guardarmovimiento','CajaController@guardarmovimiento');

Route::get('/menu/despachos','DespachosController@despacho');
Route::get('/menu/listadodespacho','DespachosController@listado');
Route::post('/generardespacho','DespachosController@guardar');

Route::get('/menu/formatocompras','ComprasController@formato');
Route::get('/menu/productocompras','ComprasController@productos');
Route::post('/menu/guardarcompra','ComprasController@guardar');

Route::get('/menu/requisiciones','RequisicionesController@formato');
Route::get('/menu/productosrequisicion','RequisicionesController@formatoproductos');
Route::post('/menu/guardarrequisicion','RequisicionesController@guardar');

Route::post('/menu/guardarcotizacion','CotizacionController@guardar');
Route::get('/listadocotizacion','CotizacionController@listado');
Route::get('/generarCOT/{cotizacion}','CotizacionController@generarPDF');

Route::get('/menu/cartera','CarteraController@cartera');
Route::get('/menu/listadocartera','CarteraController@listado');
Route::post('/generarpagos','CarteraController@guardar');
Route::get('/informeexistencias/{localizacion}','ProductosController@existencias');
Route::get('/analisiscartera','FrontController@analisiscartera');
Route::get('/historicodeventas','FrontController@historicodeventas');
Route::get('/historicodepedidos','FrontController@historicodepedidos');
Route::get('/historicodecaja','FrontController@historicodecaja');

Route::post('/listadofacturafecha','FrontController@listadofacturafecha');
Route::post('/listadopedidosfecha','FrontController@listadopedidosfecha');

Route::post('/listadocajafecha','FrontController@listadocajafecha');


Route::get('/listadopedidousuario1','PedidosController@listadopedidos1');
Route::get('/facpen','PedidosController@facturapendientes');

Route::resource('log','LogController');
Route::get('logout','LogController@logout');
