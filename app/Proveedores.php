<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\SoftDeletes;

class Proveedores extends Model
{
  use SoftDeletes;
   protected $table="proveedores";
   protected $fillable=["nit","razonsocial","direccion","telefonos","celular","email"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
