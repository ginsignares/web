<div class="panel panel-danger">
<div class="panel-heading">
	<div class="row">
		<div class="col-lg-10">	
		 <h3 class="panel-title">Estado de cuenta <br> <br> Cliente: {{$cliente->razonsocial}}</h3>
		
		 <input type='hidden' id='cliente_id' value={{$cliente->id}}>
		
		</div>
		<div class="col-lg-2">
		 
		 		<div class="form-group">
		 			<button type="button" class="btn btn-info btn-block" id="btndespachar" onclick='generardespacho()'>Pagar</button>
		 		</div>

		</div> 
	 </div>
</div>
<div class="panel-body">

       
       		<table id="tblclientes" class="table table-triped" >        		
	    				 <tr>
        					<th>
        						Factura
        					</th>
                            <th>
                                Fecha de generación
                            </th>
        					<th>
                                Subtotal
                            </th>
                            <th>
                                Descuento
                            </th>
                            <th>
        						Total
        					</th>

        					<th>
        						Abonado
        					</th>
        					<th>
        						Saldo
        					</th>
        					<th  width="8%" align="center">
        						Valor a pagar
        					</th>
        					<th  width="8%" align="center">
                                Registrar
                            </th>
                            <th  width="8%" align="center">
                                Registrado
                            </th>

        				</tr>
        				
        					<?php
        						$total=0;
        						for ($i=0; $i < count($lista); $i++) { 
        							$pendiente=($lista[$i]->total)-$lista[$i]->abonado;
        							if ($pendiente>0){
	        							echo "<tr>";
	        							echo "<td>".$lista[$i]->factura_id."</td>";
                                        echo "<td>".$lista[$i]->created_at."</td>";
                                        echo "<td>$ ".number_format($lista[$i]->total+$lista[$i]->descuento)."</td>";
                                         echo "<td>$ ".number_format($lista[$i]->descuento)."</td>";
                                        echo "<td>$ ".number_format($lista[$i]->total)."</td>";
                                        echo "<td>$ ".number_format($lista[$i]->abonado)."</td>";
	        							echo "<td>$ ".number_format($pendiente)."</td>";
	        							echo "<td><input type='text' id='Q".$lista[$i]->factura_id."' class='form-control'><input type='hidden' id='QV".$lista[$i]->factura_id."' class='form-control' value='".$pendiente."'> </td>";
	        							echo "<td><button type='button' onclick=agregar(".$lista[$i]->factura_id.") class='btn btn-success'>Registrar</button></td>";
                                        echo "<td><input type='text' id='R".$lista[$i]->factura_id."' class='form-control'  disabled=true></td>";
	        							echo "</tr>";
                                        $total+=$pendiente;
        							}
        						}
        					?>	

                        <tr>
                            <th colspan=6  style="text-align:right;">Total:</th><td colspan="4"> <?php echo "$ ".number_format($total);?> </td>
                        <tr>
          	</table>

        
    </div>
</div>        

