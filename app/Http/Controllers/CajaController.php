<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\Cajarequest;
use DB;
use Validator;
class CajaController extends Controller
{

    public function guardarmovimiento(Request $request){
        $concepto=\App\Conceptos::where('id',$request->concepto)->first();
        $saldo=$request->total-$request->valor;

        if ($saldo<0 && $concepto->tipo==2)
               return response()->json(["saldo"=>false]);
            

        $doc=new \App\Documentos;
        $doc->concepto_id=$request->concepto;
        $doc->cierre_id=$request->cierre;
        if ($concepto->tipo==1)
             $doc->valor=$request->valor;
        else
             $doc->valor=-$request->valor;
        $doc->usuario_id=\Auth::user()->id;
        $doc->detalle=$request->detalle;
        $doc->save();
        return response()->json(["valid"=>true]);
         

   } 
   public function movimientos(Request $request){
        $conceptos= DB::table('conceptos')->select()->where('tipo',$request->tipo)->where('sistema',0)->get();

         return view('caja.movimientos',['titulo'=>$request->titulo,'concepto'=>$conceptos]);

   }
  public function caja(Request $request){
        $cierre= DB::table('cierres')->select()->OrderBy('id','desc')->first();
        if ($cierre!=null){
            $documentos= DB::table('documentos')->select('documentos.id as documento_id','conceptos.nombre','documentos.detalle','documentos.valor','documentos.created_at')->join('conceptos','conceptos.id','=','documentos.concepto_id')->where('documentos.cierre_id',$cierre->id)->get();
            
            return view('caja.caja',['documentos'=>$documentos,'cierre'=>$cierre]);

        }
        else
            return view('caja.saldosiniciales');

   }
   public function cierre(Request $request){
        $cierre= \App\Cierres::find($request->cierre);
        $cierre->fechacierre=date('Y-m-d h:i:s');
        $cierre->usuariocierre_id=\Auth::user()->id;
        $cierre->save();
            
       $cierre=new \App\Cierres;
       $cierre->fechainicio= date('Y-m-d h:i:s');
       $cierre->usuario_id=\Auth::user()->id;
       $cierre->save();

        $doc=new \App\Documentos;
        $doc->concepto_id=3;
        $doc->cierre_id=$cierre->id;
        $doc->valor=$request->total;
        $doc->usuario_id=\Auth::user()->id;
        $doc->save();
        return response()->json(["valid"=>true]);

   }
public function saldo(Cajarequest $request){
        $validator= Validator::make($request->all(),$request->rules(),$request->messages());
        if ($validator->valid()){
            if ($request->ajax()){
        
               $cierre=new \App\Cierres;
               $cierre->fechainicio= date('Y-m-d h:i:s');
               $cierre->usuario_id=\Auth::user()->id;
               $cierre->save();

                $doc=new \App\Documentos;
                $doc->concepto_id=3;
                $doc->cierre_id=$cierre->id;
                $doc->valor=$request->saldo;
                $doc->usuario_id=\Auth::user()->id;
                $doc->save();

                return response()->json(["valid"=>true]);
            }
        }       
    }


}
