<div class="panel panel-primary">
<div class="panel-heading">
	<div class="row">
		<div class="col-lg-10">	
		 <h3 class="panel-title">Productos facturados <br> <br> Cliente: {{$cliente->idrazonsocial}}</h3>
		
		 <input type='hidden' id='cliente_id' value={{$cliente->id}}>
		
		</div>
		<div class="col-lg-2">
		 
		 		<div class="form-group">
		 			<button type="button" class="btn btn-warning btn-block" id="btndespachar" onclick='generardespacho()'>Despachar</button>
		 		</div>

		</div> 
	 </div>
</div>
<div class="panel-body">

       
       		<table id="tblclientes" class="table table-triped" >    
                    <thead>    		
	    				 <tr>
        					<th>
        						Factura
        					</th>
        					<th>
        						Producto
        					</th>
        					<th>
        						Cantidad
        					</th>
        					<th>
        						Terminado
        					</th>
        					<th>
        						Pendiente
        					</th>
        					<th width="3%" align="center">
        						Entregar
        					</th>
        					<th  align="center">
        						Agregar
        					</th>
        					

        				</tr>
        			</thead>
                    <tbody>	
        					<?php
        						
        						for ($i=0; $i < count($lista); $i++) { 
        							$pendiente=$lista[$i]->terminado-$lista[$i]->entregado;
        							if ($pendiente>0){
	        							echo "<tr>";
	        							echo "<td>FAC-".$lista[$i]->factura_id."</td>";
	        							echo "<td>".$lista[$i]->nombre."</td>";
	        							echo "<td>".$lista[$i]->cantidad."</td>";
	        							echo "<td>".$lista[$i]->terminado."</td>";
	        							
	        							echo "<td>".$pendiente."</td>";
	        							echo "<td><input type='text' id='Q".$lista[$i]->detalle_id."' class='form-control'><input type='hidden' id='QV".$lista[$i]->detalle_id."' class='form-control' value='".$pendiente."'> </td>";
	        							echo "<td><button type='button' onclick=agregar(".$lista[$i]->detalle_id.") class='btn btn-success'>Agregar</button></td>";
	        							echo "</tr>";
        							}
        						}
        					?>
                        </tbody>	
          	</table>
        
    </div>
</div>        

<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({

         "language":    
            {
                 "url": "/assets/js/pluginspanishtabla"
            }
    });
}); 

</script>



