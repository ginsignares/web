@extends('layouts.menu')
@section('contenido')

      <div class="panel panel-green">
            <div class="panel-heading">
                Consulta de pedidos por fecha de entrega
            </div>
            <div class="panel-body">
                       <div class="row" style="align:right">
                          <div class="col-lg-5">
                            <label><spam>Consultar pedidos con fecha de entrega desde: 
                            <input type="date" id="txtfechai" name="txtfechaf" value=<?php echo date('Y-m-d')?>> hasta 
                            <input type="date" id="txtfechaf" name="txtfechaf" value=<?php echo date('Y-m-d')?>>
                            </spam>
                            </label>
                             <button id="btnconsultar" class="btn btn-info btn-block">Consultar</button>   
                            
                          </div>  
                       </div>
                       <br>
                       <div class="row">
                             <div class="col-lg-12">
                                <table id="registro" class="table">
                                    <thead>
                                         <tr>
                                            <th>
                                                Factura
                                            </th>
                                            <th>
                                                Generada el
                                            </th>
                                            <th>
                                                Entregar el
                                            </th>
                                            <th>
                                                Cliente
                                            </th>

                                            <th>
                                                Producto
                                            </th>
                                            <th>
                                                Cantidad
                                            </th>
                                            <th>
                                                Terminados
                                            </th>
                                            <th>
                                                Pendientes
                                            </th>
                                            <th>
                                                Guardar
                                            </th>  
                                           
                                        </tr>
                                    </thead>
                                    <tbody id="fila">
                                        
                                                           
                                        
                                         
                                    </tbody>
                            </table>        
                            <table style="align:right">
                                            <?php
                                                        echo "<tr>";
                                                        echo "<td colspan=4><h1>Pedidos pendientes: </h1> </td>";
                                                        echo "<td   style='color:#FF0000'><h1 id='saldo'></h1></td>";

                                                        echo "</tr>";
                                            ?>
                                    </table>    
                            </div>
                    </div>        
            </div>    
        </div>

@endsection
@section('script')
<script type="text/javascript">
var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft  +splitRight;
 },
 new:function(num, simbol){
  this.simbol = simbol ||'';
  return this.formatear(num);
 }
}

$("#btnconsultar").click(function(){

    var fi=$("#txtfechai").val();
    var ff=$("#txtfechaf").val();

    $.ajax({    
                            url:"/controlpedidofechaentrega",
                            type:'POST',
                            data:{fi:fi,ff:ff},
                            dataType:"json",
                            success: function(resp){    
                               
                                var tabla=$("#fila");        
                                var acumulatotal=0;
                                tabla.html('');
                                  console.log(resp.lista.length);
                                for (var i =0; i < resp.lista.length; i++) {
                                  
                                  
                                    var pendientes=resp.lista[i].cantidad-resp.lista[i].terminado;
                                    acumulatotal+=pendientes;


                                    if (pendientes>0)

                                     tabla.append("<tr><td>"+resp.lista[i].factura_id+"</td><td>"+resp.lista[i].created_at+"</td><td>"+resp.lista[i].fechaentrega+"</td><td>"+resp.lista[i].razonsocial+"</td><td>"+resp.lista[i].nombre+"</td><td>"+resp.lista[i].cantidad+"</td><td>"+resp.lista[i].terminado+"</td><td><input style='width:50px;text-align:center' id='Q"+resp.lista[i].detalle_id+"' hidden name='Q"+resp.lista[i].detalle_id+"' type='text' value="+pendientes+"> <input style='width:50px;text-align:center' id='Qp"+resp.lista[i].detalle_id+"' name='Qp"+resp.lista[i].detalle_id+"' type='text' value="+pendientes+"></td><td><input type='button' value='Guardar' class='btn-info' onclick='btnguardar("+resp.lista[i].detalle_id+")'></td></tr>");  
                                   else

                                     tabla.append("<tr><td>"+resp.lista[i].factura_id+"</td><td>"+resp.lista[i].created_at+"</td><td>"+resp.lista[i].fechaentrega+"</td><td>"+resp.lista[i].razonsocial+"</td><td>"+resp.lista[i].nombre+"</td><td>"+resp.lista[i].cantidad+"</td><td>"+resp.lista[i].terminado+"</td><td>No</td><td></td></tr>");  

                                }
                                $("#saldo").html(" "+acumulatotal)

                            },
                            error: function(jqXHR,estado,error){
                                alert(error);

                            },
                            complete:function(jqXHR,estado){
                               
                            }
                    });


});
function btnguardar(idtxt){

  
  var Q=Number($("#Q"+idtxt).val());
  var Qp=Number($("#Qp"+idtxt).val());
  if (Qp<=0){
        $.notify("La cantidad ingresada no es valida","error");
        return;
      }
  
   if (Qp>Q){
        $.notify("La cantidad ingresada no es valida","error");
        return;
      }
  
    swal({
          title:"!Advertencia¡",
          text:"¿Esta seguro de realizar los cambios en el sistema?",
          showCancelButton:true,
          type:"warning",
          confirmButtonColor:'#e92c43',
          cancelButtonColor:'#e92c43',
          confirmButtonText:"Aceptar"
    }, function (isConfirm){  
      
      if(isConfirm)
      {
        $.ajax({  
            url:"/actulizarproducto",
            type:'post',
            data:{detalle_id:idtxt,cantidad:Qp},
            dataType:"html",
            success: function(resp){
                    
                    $.notify("Los cambios se han sido realizados con exito","success");
                    $(location).attr('href','/listadofacturasporfechapedido');
                  
            },
            error: function(jqXHR,estado,error){
              $.notify(error,"error");

            },
            complete:function(jqXHR,estado){
            
            }
        });

      } 


    });
}

</script>
@endsection 
