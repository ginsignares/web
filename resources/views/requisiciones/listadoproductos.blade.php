@extends('layouts.listadoproductosfactura')
@section('contenido')
<div class="row">
<br>
    <div class="col-lg-8">

      <div class="panel panel-green">
            <div class="panel-heading">
                Productos
            </div>
            <div class="panel-body">
        			<table id="tblclientes" class="table table-triped">
        			  <thead>		
        				<tr>
        					<th width="50%">
        						Producto
        					</th>
        					<th  width="30%">
        						Disponible
        					</th>
        					<th>
        						Entregar
        					</th>
        					<th  align="center">
        						Seleccionar
        					</th>
        				</tr>
        			  </thead>	
        			  <tbody>
        				
        					<?php
        						
        						for ($i=0; $i < count($lista); $i++) { 
        							echo "<tr>";
        							echo "<td>".$lista[$i]->nombre."</td><td><input type='hidden' id='QV".$lista[$i]->producto_id."' value='".$lista[$i]->saldos."'>".$lista[$i]->saldos."</td><td><input id='Q".$lista[$i]->producto_id."' type='text' class='form-control'></td><td><label onclick=consultar(".$lista[$i]->producto_id.") class='btn btn-success btn-block'><i class='fa fa-check-circle'> Agregar</label></td>";
        							echo "</tr>";
        						}
        					?>	
        			</tbody>
        		</table>
        		       		</div>    
            <div class="panel-body" align="right">
             <button type="button" class="btn btn-default" style='width: 100px' data-dismiss="modal">Cancelar</button>
            </div>
        </div>

</div>
</div>
        							
@endsection
@section('script')
<script type="text/javascript">
$(document).ready(function(){
    $("#tblclientes").DataTable({

         "language":    
            {
                 "url": "/assets/js/pluginspanishtabla"
            }
    });
});
function validarproducto(id){
	var sw=-1;
	for (var i = 0; i < productos.length; i++) {
		if (Number(productos[i].producto_id)==id)
		{
			sw=i;
			console.log('Posicion: '+sw);
		
			break;
		}
	}
	return sw;
};
function consultar(id){
	var Q=Number($("#Q"+id).val());
	var QV=Number($("#QV"+id).val());
	var	valida=validarproducto(id);
	console.log(valida);
	

	if ((isNaN(Q)==true) | (Q<0) | (Q==0)){
			$("#Q"+id).focus();
			$.notify("La cantidad ingresada no es valida","error");
			return;
	}
	if (valida==-1)
		var saldo=Q;
	else	
	{	
		if (productos[valida].estado==1)
			var saldo=Q;
		else	
			var saldo=productos[valida].cantidad+Q;
	}	
	if (saldo>QV){
		$("#Q"+id).focus();
		$.notify("La cantidad ingresada supera el existencia del inventario","error");
		return;
	}
	
	if (valida==-1){

		$.ajax({	
					url:"/obtenerproducto",
					type:'post',
					data:{producto_id:id},
					dataType:"json",
					success: function(resp){
						productos.push({producto_id:resp[0].id,nombre:resp[0].nombre,cantidad:Q,precio:resp[0].precio,estado:0});
						console.log(productos);
					},
					error: function(jqXHR,estado,error){
						$.notify(error,"error");

					},
					complete:function(jqXHR,estado){
						cargarproductos();
					}
			});
	}
	else
	{
		if (productos[valida].estado==1){
			productos[valida].cantidad=Q;
			productos[valida].estado=0;	
		} 
		else
			productos[valida].cantidad+=Q;
		cargarproductos();
	}	
}

</script>
@endsection

