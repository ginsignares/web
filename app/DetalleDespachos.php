<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetalleDespachos extends Model
{
    protected $table="detalledespacho";
    protected $fillable=["despacho_id","detalle_id","cantidad"];
    public $timestamps = false;
}
