<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReporteEstadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reporte_estados', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('fechahora');
            $table->integer('estado_id')->unsigned();
            $table->integer('etapa_id')->unsigned();
            $table->integer('usuario_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reporte_estados');
    }
}
