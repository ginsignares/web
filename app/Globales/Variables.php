<?php
namespace App\Globales;
use App\Http\Controllers\Controller;
use Auth;
use DB;
Class Variables extends Controller{
    public function __construct(){
         $this->middleware('auth');
    }
	
	public function obtenerempresa(){
		$tabla = DB::table('users_empresas')->select('empresa_id')->where('user_id',Auth::user()->id)->first();
		return $tabla->empresa_id;

	}
}