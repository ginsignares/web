@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
 <div class="col-lg-12">

	{!!Form::open(['id'=>'frmproducto'])!!} 
        		

 	
	<div class="panel panel-green">
        <div class="panel-heading">
            <h3 class="panel-title">Productos</h3>
        </div>
        <div class="panel-body">
        			
        			<div class="form-group">
        					<input type="hidden" class="form-control" id="producto_id" name="producto_id" value=0>
	            			<label>Codigo</label>
						    <input type="text" class="form-control" id="codigo" name="codigo">

						   
				    </div>
	            	<div class="form-group">
	            			<label>Nombre</label>
						    <input type="text" class="form-control" id="nombre" name="nombre">
						   
				    </div>
				    <div class="form-group">
	            			<label>Precio</label>
						    <input type="text" class="form-control" id="precio" name="precio">
						   
				    </div>
				    <div class="form-group">
	            			<label>Fabricado</label>
						    <select id="fabricado" name="fabricado" class="form-control">
						    	<option value="Si">Si</option>
						    	<option value="No">No</option>
						    </select>	
						   
				    </div>

				    <div class="form-group">
	            			<label>Ubicacion</label>
						    <input type="text" class="form-control" id="ubicacion" name="ubicacion">
						   
				    </div>
				    <div class="form-group">
	            			<label>Descripción</label>
						    <textarea class="form-control" id="descripcion" name="descripcion"></textarea>
						   
				    </div>
				     
	      
        </div>
        <div class="panel-footer">
        	<div class="form-group"  align="right">
        	  <button type="button" id="consultar"   data-toggle="modal" data-target="#VentanaModal" class="btn btn-default">Consultar</button>
        	 <button type="submit" class="btn btn-success">Guardar</button>
        	</div> 
        </div>
    </div>
   {!!Form::close()!!}
 </div>
</div>

@endsection
@section('script')
	{!!Html::script('assets/js/producto.js')!!}
@endsection	
	