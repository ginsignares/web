var productos= new Array();
var subtotal=0;
var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
  num +='';
  var splitStr = num.split('.');
  var splitLeft = splitStr[0];
  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
  var regx = /(\d+)(\d{3})/;
  while (regx.test(splitLeft)) {
  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
  }
  return this.simbol + splitLeft  +splitRight;
 },
 new:function(num, simbol){
  this.simbol = simbol ||'';
  return this.formatear(num);
 }
}
function grabaritem(id){
	var Q=$("#Q"+id).val();
	var Qp=$("#Qp"+id).val();
	
		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
			if (Q>Qp || Q<0){
				$.notify("La cantidad ingresada no es valida","error")
			}
			else
			{
				$.ajax({	
						url:"/actulizarproducto",
						type:'post',
						data:{detalle_id:id,cantidad:Q},
						dataType:"html",
						success: function(resp){
								    
										$.notify("Los cambios se han sido realizados con exito","success");
										$(location).attr('href','/controlpedido');
									
						},
						error: function(jqXHR,estado,error){
							$.notify(error,"error");

						},
						complete:function(jqXHR,estado){
						
						}
				});

			}	


		});
}


function consultar(id){
		$.ajax({	
			url:"/menu/formatoventas",
			type:'get',
			data:{cliente_id:id,formato_id:0},
			dataType:"html",
			success: function(resp){
				 $("#viewformato").html(resp)
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
} 
$("#btnguardar").click(function(){
	var cliente=$("#cliente_id").val();
	var formadepago=$("#formapago").val();
	var descuento=$("#descuento").val();


	swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){	
			if(productos.length==0){
			
				$.notify("La factura no contiene productos para ser creada","error");		
			}
			else{
			
				$.ajax({	
					url:'/menu/guardarventa',
					type:'POST',
					data:{cliente_id:cliente,productos:productos,valor:subtotal,descuento:descuento,formapago:formadepago},
					success: function(resp){
						if (resp.valid !==undefined){
							$.notify("Los cambios se han sido realizados con exito","success");
							window.open('/generarPDF/'+resp.factura, '_blank');
							$(location).attr('href','/menu/ventas');
						}	
						else
						{
							$.notify("La caja esta cerrada para realiar esta venta","error");
							$(location).attr('href','/menu/ventas');
						}				
					},
					error: function(jqXHR,estado,error){
						$.notify(error,"error");

					},
					complete:function(jqXHR,estado){
					
					}
			
				});
			}	
		});

});


$("#btnagregar").click(function(){
		$.ajax({	
			url:"/menu/formatoproductos",
			type:'get',
			data:{formato_id:0},
			dataType:"html",
			success: function(resp){
				 $("#VentanaModal").html("");
				 $("#VentanaModal").html(resp);
			},
			error: function(jqXHR,estado,error){
				$.notify(error,"error");

			},
			complete:function(jqXHR,estado){
			
			}
	});
});

$("#btncancelar").click(function(){
	$(location).attr('href','/menu/ventas');
});
$("#btncalculardescuento").click(function(){
	var Q= $("#descuento").val();

	if ((isNaN(Q)==true) | (Q<0) | (Q==0 ) | (Q>subtotal)){
			$("#descuento").focus();
			$("#descuento").val('');
			$("#subtotal").val(formatNumber.new(subtotal,"$ "));
			$("#total").val(formatNumber.new(subtotal,"$ "));
			$.notify("La cantidad ingresada no es valida","error");
			return;
	}
	var neto=subtotal-Q;
	$("#total").val(formatNumber.new(neto,"$ "));

});



function cargarproductos(){

	$("#cpdetallepedido").html("");
	var totalproductos=productos.length;
	var posicion=1;
	var total=0;
	subtotal=0;

	for (var i = 0; i < totalproductos; i++) {
	   if (productos[i].estado=="0"){	
		total=productos[i].cantidad*productos[i].precio;
		$("#cpdetallepedido").append("<tr><td>"+posicion+"</td><td width='60%'>"+productos[i].nombre+"</td><td>"+productos[i].cantidad+"</td><td>"+formatNumber.new(productos[i].precio,"$ ")+"</td><td>"+formatNumber.new(total,"$ ")+"</td><td><label onclick=borrar("+i+") class='btn btn-danger btn-block'><i class='fa fa-times-circle'> Quitar</label></td></tr>");
		posicion++;
		subtotal+=total;
	   }	

	}
	$("#subtotal").val(formatNumber.new(subtotal,"$ "));
	$("#total").val(formatNumber.new(subtotal,"$ "));

}
function borrar(id){
	productos[id].estado="1";
	cargarproductos();
}