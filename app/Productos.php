<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Productos extends Model
{
     use SoftDeletes;
   protected $table="productos";
   protected $fillable=["codigo","nombre","precio","fabricado","ubicacion","descripcion"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];

}
