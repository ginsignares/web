<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\SoftDeletes;

class Pedidos extends Model
{
 use SoftDeletes;
   protected $table="pedidos";
   protected $fillable=["cliente_id","fechaentrega","observaciones","estado"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
