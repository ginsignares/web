<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saldos extends Model
{
   protected $table="saldos";
   protected $fillable=["id","producto_id"];
}
