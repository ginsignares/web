<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaquinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maquinas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo');
            $table->string('referencia');
            $table->string('equipo');
            $table->string('serie');
            $table->string('marca');
            $table->string('garantia');
            $table->string('modelo');
            $table->string('logo');
            $table->dateTime('adquisicion');
            $table->integer('etapa_id')->unsigned();
            $table->integer('tipo_id')->unsigned();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('maquinas');
    }
}
