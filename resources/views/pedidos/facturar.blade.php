@extends('layouts.menu')
@section('contenido')
<div id="VentanaModal" class="modal fade" tabindex="0" role="dialog" ></div>
<div class="row">
	 <div id="viewformato" class="col-lg-12">			
		<div class="panel panel-green">
	        <div class="panel-heading">
	            <h3 class="panel-title">Facturar Pedidos</h3>
	        </div>
	        <div class="panel-body">
	        	<div class="row">
	 				<div class="col-lg-12">
	 					<div class="panel panel-info">
			 					<div class="panel-heading">
			           				 <h3 class="panel-title">Seleccionar cliente</h3>
			        			</div>
			 					<div class="panel-body">
								         <div class="table-responsive">
								       		<table id="tblclientes" class="table table-triped" >        		
									    				 <tr>
								        					<th>
								        						Razón social
								        					</th>
								        					<th>
								        						Teléfonos
								        					</th>
								        					<th>
								        						Email
								        					</th>
								        					<th width="10%" align="center">
								        						Seleccionar
								        					</th>
								        				</tr>
								        				
								        					<?php
								        						
								        						for ($i=0; $i < count($lista); $i++) { 
								        							echo "<tr>";
								        							echo "<td>".$lista[$i]->razonsocial."</td><td>".$lista[$i]->telefonos." ".$lista[$i]->celular."</td><td>".$lista[$i]->email."</td><td><label onclick=consultar(".$lista[$i]->id.") class='btn btn-info btn-block'><i class='fa fa-check-circle'> Seleccionar</label></td>";
								        							echo "</tr>";
								        						}
								        					?>	
								          	</table>
								        </div>
								    </div>
					    </div>
				    </div>
				</div>        
	    </div>
	   {!!Form::close()!!} 
	 </div>
	
</div>
</div>

{!!Html::script('assets/js/jquery.min.js')!!}
{!!Html::script('assets/js/facturapedidos.js')!!}
@endsection	

