<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\SoftDeletes;

class Clientes extends Model
{

  use SoftDeletes;
   protected $table="clientes";
   protected $fillable=["nit","razonsocial","direccion","telefonos","celular","email"];
   protected $dates=["deleted_at"];
   protected $hidden=["deleted_at"];
}
