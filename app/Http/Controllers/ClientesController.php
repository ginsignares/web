<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\Clientesrequest;
use Validator;

class ClientesController extends Controller
{
   public function __construct(){
            $this->middleware('auth');
   }

   public function Listado(){
   		$datos=\App\Clientes::where('deleted_at',null)->get();
   		return view('configuracion.listadoclientes',['lista'=>$datos]);
   }

   public function guardar(Clientesrequest $request){
    	$validator= Validator::make($request->all(),$request->rules(),$request->messages());
        if ($validator->valid()){
            if ($request->ajax()){
                $datos=\App\Clientes::where('nit',$request['nit'])->where('deleted_at',null)->first();
                if ($datos!=null)
                    return response()->json(["nit"=>'El nit ya existe']);

                $datos=\App\Clientes::where('razonsocial',$request['razonsocial'])->where('deleted_at',null)->first();
                if ($datos!=null)
                    return response()->json(["razonsocial"=>'La razonsocial ya existe']);
    	
		    	$datos=new \App\Clientes;
		    	$datos->nit=$request['nit'];
		    	$datos->razonsocial=$request['razonsocial'];
		    	$datos->direccion=$request['direccion'];
		    	$datos->telefonos=$request['telefonos'];
		    	$datos->celular=$request['celular'];
		    	$datos->email=$request['email'];
		    	$datos->save();

		    	return response()->json(["valid"=>true]);
		    }
		}   	
    }

    public function update(Clientesrequest $request){

        $validator= Validator::make($request->all(),$request->rules(),$request->messages());
        if ($validator->valid()){
            if ($request->ajax()){
                $datos=\App\Clientes::where('nit',$request['nit'])->where('deleted_at',null)->first();
                if ($datos!=null){
                    if ($datos->id!=$request['cliente_id'])
                    return response()->json(["nit"=>'El nit ya existe']);
                }

                $datos=\App\Clientes::where('razonsocial',$request['razonsocial'])->where('deleted_at',null)->first();
                if ($datos!=null){
                    if ($datos->id!=$request['cliente_id'])
                        return response()->json(["razonsocial"=>'La razonsocial ya existe']);
                }

            	$datos=\App\Clientes::find($request['cliente_id']);
            	$datos->nit=$request['nit'];
            	$datos->razonsocial=$request['razonsocial'];
            	$datos->direccion=$request['direccion'];
            	$datos->telefonos=$request['telefonos'];
            	$datos->celular=$request['celular'];
            	$datos->email=$request['email'];
            	$datos->save();

    	        return response()->json(["valid"=>true]);
            }
        }    

    }

    public function borrar(Request $request){
        $datos=\App\Clientes::find($request['cliente_id']);
        $datos->deleted_at= date('Y-m-d H:s:i');
        $datos->save();
        return response()->json(["valid"=>true]);
    }
}
