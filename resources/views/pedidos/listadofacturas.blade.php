@extends('layouts.menu')
@section('contenido')

      <div class="panel panel-green">
            <div class="panel-heading">
                Pedidos
            </div>
            <div class="panel-body">
                        <table id="registro" class="table">
                            <thead>
                                 <tr>
                                    <th>
                                        Producto
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th width="10%" align="center">
                                        Ver
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            
                                <?php
                                    $saldo=0;
                                    for ($i=0; $i < count($lista); $i++) { 
                                      echo "<tr>";
                                        echo "<td>".$lista[$i]['nombre']."</td>";
                                        echo "<td>".$lista[$i]['saldo']."</td>";
                                        echo "<td><button class='btn btn-info btn-block     ' onclick='consultar(".$lista[$i]['producto_id'].")'> Ver</button></td>";
                                        echo "</tr>";
                                        $saldo+=$lista[$i]['saldo'];    
                                    }

                                ?>  
                            </tbody>
                    </table>        
                    <table style="align:right">
                                    <?php
                                                echo "<tr>";
                                                echo "<td colspan=4><h1>Total: </h1> </td>";
                                                echo "<td style='color:#FF0000'><h1>".number_format($saldo)."</h1></td>";

                                                echo "</tr>";
                                    ?>
                            </table>    
            </div>    
        </div>

@endsection
@section('script')
  <script type="text/javascript">
$(document).ready(function(){
    $("#registro").DataTable({
         "language":    
            {
                 "url": "/assets/js/pluginspanishtabla"
            }
    });
});
function consultar(id){
    
    $(location).attr('href',"/controlpedido/"+id);
}
</script>
@endsection 

        