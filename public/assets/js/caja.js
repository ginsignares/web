$(function(){
   $("#frmsaldos").on("submit", function(e){
   		var datos= $("#cliente_id").val();
   		var url="/grabarsaldocaja";
   	 	var fields= $(this).serialize();
   		
   			

  		console.log(url+" "+fields);
   		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){

			if (isConfirm){
					$.ajax({	
							url:url,
							type:'POST',
							data:fields,
							 dataType: "json",
							success: function(resp){	
								if (resp.valid !==undefined){
									$.notify("Los cambios se han sido realizados con exito","success");
									$(location).attr('href','/menu/caja');
									$("#frmsaldos")[0].reset();
								}
								else
								{	
									if(resp.saldo!==undefined){
										$("#saldo").focus();
										$.notify(resp.saldo,"error");
										return false;
									}
								}		
							},
							error: function(jqXHR,estado,error){
								$.notify(error,"error");

							},
							complete:function(jqXHR,estado){
							
							}
					});



			}

		});


		
   		return false;        
    });
});

$("#btncierrecaja").click(function(){
		swal({
					title:"!Advertencia¡",
					text:"¿Esta seguro de realizar los cambios en el sistema?",
					showCancelButton:true,
					type:"warning",
					confirmButtonColor:'#e92c43',
					cancelButtonColor:'#e92c43',
					confirmButtonText:"Aceptar"
		}, function (isConfirm){
				var cierre=$("#cierre").val();
				var total=$("#total").val();
				$.ajax({	
							url:'/cierrecaja',
							type:'POST',
							data:{cierre:cierre,total:total},
							dataType: "json",
							success: function(resp){	
								if (resp.valid !==undefined){
									$.notify("Los cambios se han sido realizados con exito","success");
									$(location).attr('href','/menu/caja');
								}
								
							},
							error: function(jqXHR,estado,error){
								$.notify(error,"error");

							},
							complete:function(jqXHR,estado){
							
							}
					});



		});
});
$("#btningresos").click(function(){
	$.ajax({	
							url:'/movimientos',
							type:'get',
							data:{tipo:1,titulo:'Ingresos a caja'},
							dataType: "html",
							beforeSend: function(){
								$("#VentanaModal").html('');
								
							},
							success: function(resp){	
								$("#VentanaModal").html(resp);
								
							},
							error: function(jqXHR,estado,error){
								$.notify(error,"error");

							},
							complete:function(jqXHR,estado){
							
							}
					});

});
$("#btnegresos").click(function(){
	$.ajax({	
							url:'/movimientos',
							type:'get',
							data:{tipo:2,titulo:'Ingresos a caja'},
							dataType: "html",
							beforeSend: function(){
								$("#VentanaModal").html('');

							},
							success: function(resp){	
								$("#VentanaModal").html(resp);
								
							},
							error: function(jqXHR,estado,error){
								$.notify(error,"error");

							},
							complete:function(jqXHR,estado){
							
							}
					});
});
